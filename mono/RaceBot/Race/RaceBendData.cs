﻿    using System;
    using System.Collections;
    using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaceBot.Race
{
    public struct BendProperties
    {
        public double Steepness;
        public double DesiredVelocity;
        public double LateBrakeDistance;
        public double OutOfCornerAccelerateDistance;


        public static BendProperties InterpolateProperties(BendProperties first, BendProperties second, double desiredSteepness)
        {
            // use linear interpolation:
            // ax + b = y
            // y = y0 + (y1 -y0) * (x - x0) / (x1 - x0)
            var bend = new BendProperties();

            bend.DesiredVelocity = (second.DesiredVelocity - first.DesiredVelocity) *
                                   (desiredSteepness - first.Steepness) / (second.Steepness - first.Steepness)
                                   + first.DesiredVelocity;

            bend.OutOfCornerAccelerateDistance = (second.OutOfCornerAccelerateDistance - first.OutOfCornerAccelerateDistance) *
                                   (desiredSteepness - first.Steepness) / (second.Steepness - first.Steepness)
                                   + first.OutOfCornerAccelerateDistance;

            bend.LateBrakeDistance = (second.LateBrakeDistance - first.LateBrakeDistance) *
                                   (desiredSteepness - first.Steepness) / (second.Steepness - first.Steepness)
                                   + first.LateBrakeDistance;
            return bend;
        }
    }

    public class BendPropertyComparer : IComparer<BendProperties>
    {
        public int Compare(BendProperties x, BendProperties y)
        {
            return x.Steepness.CompareTo(y.Steepness);
        }
    }


    public class RaceBendData
    {
        // requires at least 2 instances
        private readonly List<BendProperties> bendProperties; 

        public RaceBendData(List<BendProperties> bendProperties)
        {
            this.bendProperties = bendProperties;
            // We need the bends sorted for quick interpolation between values that are not found 
            // in the pre-existing data
            bendProperties.Sort(new BendPropertyComparer());
        }


        public BendProperties GetBendValues(double steepness)
        {
            if (steepness < bendProperties[0].Steepness)
            {
                return BendProperties.InterpolateProperties(bendProperties[0], bendProperties[1], steepness);
            }
            for (int i = 1; i < bendProperties.Count; i++)
            {
                var bend = bendProperties[i];
                if (Math.Abs(steepness - bend.Steepness) <= 1)
                    return bend;
                if (steepness > bendProperties[i - 1].Steepness && steepness < bend.Steepness)
                {
                    return BendProperties.InterpolateProperties(bendProperties[i - 1],
                        bend, steepness);
                }
                
            }


            return BendProperties.InterpolateProperties(bendProperties[bendProperties.Count - 2],
                bendProperties[bendProperties.Count - 1], steepness);
        }
    }
}
