using System;
using System.IO;
using System.Net.Sockets;
using RaceBot.Messages.Send;

namespace RaceBot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string host = args[0];
            int port = int.Parse(args[1]);
            string botName = args[2];
            string botKey = args[3];

            string trackName;
            string ai = "SpinRacer";
            string joinOrCreate = "join";
            int carCount = 1;
            string password = "";


            if(args.Length > 5)
                ai = args[5];
            if (args.Length > 6)
                joinOrCreate = args[6];
            if (args.Length > 7)
                carCount = int.Parse(args[7]);    
            if (args.Length > 8)
                password = args[8]; 
  
            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
            if(args.Length < 5)
                Console.WriteLine ("Chosen track: no track chosen");
            else
                Console.WriteLine ("Chosen track: " + args[4]);

            using (var client = new TcpClient(host, port))
            {
                NetworkStream stream = client.GetStream();
                var reader = new StreamReader(stream);
                var writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                Join botJoin;

                if (args.Length < 5) 
                    botJoin = new Join (botName, botKey);
                else
                {
                    trackName = args[4];
                    if (joinOrCreate == "join")
                        botJoin = new JoinRace(botName, botKey, trackName, carCount, password);
                    else if (joinOrCreate == "create")
                        botJoin = new CreateRace(botName, botKey, trackName, carCount, password);
                    else
                        botJoin = new JoinRace(botName, botKey, trackName, carCount, password);
                }

                var bot = new Bot( writer, botJoin, ai);

                bot.Play(reader);
            }

            // Keep the console window open in debug mode.
#if DEBUG
            //Console.WriteLine("Press any key to exit.");
            //Console.ReadKey();
#endif
        }
    }
}