﻿using SEdge.Common;

namespace SEdge.AI
{
    public interface IAiState
    {
        /// <summary>
        /// Setting this to true causes AI to continue executing after handling this state
        /// </summary>
        bool PassThrough { get; }

        /// <returns>
        /// values higher than 0 imply that the state could be active.
        /// </returns>
        double Value { get; }

        /// <summary>
        /// Calculates arbitrary value that implies how much this state wishes to activate
        /// in the current state of game
        /// </summary>
        void CalculateDecisionValue();


        /// <summary>
        /// Executes this state
        /// </summary>
        /// <param name="zTime"></param>
        void Execute(IZTime zTime);
    }
}
