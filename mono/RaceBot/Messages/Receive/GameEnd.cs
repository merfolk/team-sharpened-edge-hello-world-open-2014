﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RaceBot.Messages.Receive
{
	/// <summary>
	/// "When the game is over, the server sends the gameEnd messages that contains race results."
	/// </summary>
	public class GameEnd
	{
		public List<ResultPairs> results { get; set; }
		public List<BestLap> bestLaps { get; set; }

		public override string ToString ()
		{
			StringBuilder builder = new StringBuilder ();

			builder.AppendLine ();
            builder.AppendLine ("GAME END");
			builder.AppendLine ();
            builder.AppendLine("STANDINGS");

			for (int i = 0; i < results.Count; i++) {
                builder.AppendFormat ("{0}. {1} ({2}), total {3} laps, {4} seconds",
					i + 1,
					results[i].car.name,
					results[i].car.color,
					results[i].result.laps,
					results[i].result.millis / 1000f
				);
				builder.AppendLine ();
			}

            builder.AppendLine ();
            builder.AppendLine("FASTEST LAPS");
            for (int i = 0; i < results.Count; i++)
            {
                builder.AppendFormat("{0}. {1} ({2}), best lap {3} (lap {4})",
                    i+1,
                    bestLaps[i].car.name,
                    bestLaps[i].car.color,
                    bestLaps[i].result.millis / 1000f,
                    bestLaps[i].result.lap + 1);
                builder.AppendLine ();
            }

			return builder.ToString ();
		}
	}

	public class ResultPairs
	{
		public CarId car { get; set; }
		public Result result { get; set; }
	}

	public class Result
	{
		public int laps { get; set; }
		public int ticks { get; set; }
		public int millis { get; set; }
	}

	public class BestLap
	{
		public CarId car { get; set; }
        public BestLapResult result { get; set; }
	}

    public class BestLapResult
    {
        public int lap { get; set; }
        public int ticks { get; set; }
        public int millis { get; set; }
    }
}

