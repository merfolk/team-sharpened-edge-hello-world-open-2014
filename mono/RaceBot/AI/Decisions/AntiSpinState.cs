﻿using System;
using RaceBot.Race;
using RaceBot.Utilities;
using SEdge.Common;

namespace RaceBot.AI.Decisions
{
    public class AntiSpinState : SpeedStopperPrime
    {
        //private double throttle;
        //private readonly int crashCheckTicks;
        //private readonly int antiCrashCheckTicks;

        public AntiSpinState(RaceKnowledge knowledge, int predictTicks, int ticksOnThrottle, SpinMother spinMother)
            : base(knowledge, predictTicks, ticksOnThrottle, spinMother)
        {
            //antiCrashCheckTicks = predictTicks;
        }


        public override void CalculateDecisionValue()
        {
            if (spinMother != null)
            {
                if (MyCar.CurrentTrackPiece.IsBend() || Math.Abs(MyCar.VelocityDelta) > 50
                    || MyCar.CurrentVelocity < 200)
                {
                    spinMother.BreakingToCorner = false;
                }
            }
            //predictTicks = antiCrashCheckTicks;
            Value = 0;
            Throttle = 0;
            endOfTurnDistance = 0;
            if (WillCrashOnSpeed(Straigth))
            {
                Throttle = 1;
                //Value = 1;
                
                
                if (!WillCrashOnSpeed(Straigth, (60.00 + RaceMath.CrashAngle)/2))
                {
                    Value = 1;
                    return;
                }
                //else
                //{
                //    if (Math.Abs(lastMax) <= Math.Abs(endOfTurnDistance))
                //        Value = 1;
                //}
                //var currentTrackPiece = knowledge.MyCar.CurrentTrackPiece;
                //var piece = knowledge.MyCar.CurrentTrackPiece;
                //if (MyCar.CurrentTrackPiece.LastTickOnPiece(MyCar))
                //{
                //    // doesn't matter what we do on this piece anymore
                //    currentTrackPiece = next;
                //    next = next.GetNext();
                //}

                //for (int i = 0; i < knowledge.Track.Pieces.Count; i++)
                //{
                //    if (maxAngle > 0 && !piece.IsRightTurn())
                //    {
                //        Value = 1;
                //    }
                //    else if (maxAngle < 0 && !piece.IsLeftTurn())
                //    {
                //        Value = 1;
                //    }
                //    if (crashIndex == piece.Index)
                //        return;
                //    piece.GetNext();
                //}

                //if (crashIndex >= currentTrackPiece.Index &&
                //    crashIndex <= next.Index)
                //{
                //    if (maxAngle > 0 && !currentTrackPiece.IsRightTurn() || !next.IsRightTurn())
                //    {
                //        Value = 1;
                //    }
                //    else if (maxAngle < 0 && !currentTrackPiece.IsLeftTurn() || !next.IsLeftTurn())
                //    {
                //        Value = 1;
                //    }
                //}
            }

        }
        public override void Execute(IZTime zTime)
        {

            knowledge.Throttle = 1;

            Log(zTime, "AS" + ticksOnFullSpeed, ZLib.FloatToText(maxAngle));
        }
    }
}