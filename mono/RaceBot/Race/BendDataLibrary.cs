﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaceBot.Race
{
    public static class BendDataLibrary
    {
        public static List<BendProperties> AddBendsSpinRacer()
        {
            List<BendProperties> bendProperties = new List<BendProperties>();
            bendProperties.Add(new BendProperties()
            {
                Steepness = 143.24,
                DesiredVelocity = 275,
                LateBrakeDistance = 0,
                OutOfCornerAccelerateDistance = 41.88774085
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 95.49,
                DesiredVelocity = 325,
                LateBrakeDistance = 0,
                OutOfCornerAccelerateDistance = 62.83380459
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 63.66,
                DesiredVelocity = 390,
                LateBrakeDistance = 0,
                OutOfCornerAccelerateDistance = 94.25070688
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 52.09,
                DesiredVelocity = 420,
                LateBrakeDistance = 0,
                OutOfCornerAccelerateDistance = 100
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 30.16,
                DesiredVelocity = 550,
                LateBrakeDistance = 0,
                OutOfCornerAccelerateDistance = 100
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 27.28,
                DesiredVelocity = 550,
                LateBrakeDistance = 0,
                OutOfCornerAccelerateDistance = 100
            });
            return bendProperties;
        }

        public static List<BendProperties> AddBendsPrime()
        {
            List<BendProperties> bendProperties = new List<BendProperties>();
            bendProperties.Add(new BendProperties()
            {
                Steepness = 143.24,
                DesiredVelocity = 260,
                LateBrakeDistance = 20.94387043,
                OutOfCornerAccelerateDistance = 41.88774085
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 95.49,
                DesiredVelocity = 310,
                LateBrakeDistance = 31.41690229,
                OutOfCornerAccelerateDistance = 62.83380459
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 63.66,
                DesiredVelocity = 380,
                LateBrakeDistance = 47.12535344,
                OutOfCornerAccelerateDistance = 94.25070688
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 52.09,
                DesiredVelocity = 425,
                LateBrakeDistance = 50,
                OutOfCornerAccelerateDistance = 100
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 26.04,
                DesiredVelocity = 550,
                LateBrakeDistance = 50,
                OutOfCornerAccelerateDistance = 100
            });

            return bendProperties;
        }

        public static List<BendProperties> AddBendsOld()
        {
            List<BendProperties> bendProperties = new List<BendProperties>();
            bendProperties.Add(new BendProperties()
            {
                Steepness = 143.24,
                DesiredVelocity = 266,
                LateBrakeDistance = 20.94387043,
                OutOfCornerAccelerateDistance = 41.88774085
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 95.49,
                DesiredVelocity = 318,
                LateBrakeDistance = 31.41690229,
                OutOfCornerAccelerateDistance = 62.83380459
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 63.66,
                DesiredVelocity = 380,
                LateBrakeDistance = 47.12535344,
                OutOfCornerAccelerateDistance = 94.25070688
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 52.09,
                DesiredVelocity = 425,
                LateBrakeDistance = 50,
                OutOfCornerAccelerateDistance = 100
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 26.04,
                DesiredVelocity = 550,
                LateBrakeDistance = 50,
                OutOfCornerAccelerateDistance = 100
            });

            return bendProperties;
        }

        


        public static List<BendProperties> AddBendsEarlyBrake()
        {
            List<BendProperties> bendProperties = new List<BendProperties>(); 
            bendProperties.Add(new BendProperties()
            {
                Steepness = 143.24,
                DesiredVelocity = 270,
                LateBrakeDistance = 3.490645071,
                OutOfCornerAccelerateDistance = 41.88774085
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 95.49,
                DesiredVelocity = 320,
                LateBrakeDistance = 5.236150382,
                OutOfCornerAccelerateDistance = 62.83380459
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 63.66,
                DesiredVelocity = 370,
                LateBrakeDistance = 7.854225573,
                OutOfCornerAccelerateDistance = 94.25070688
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 52.09,
                DesiredVelocity = 400,
                LateBrakeDistance = 9.598771357,
                OutOfCornerAccelerateDistance = 115.1852563
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 30.16,
                DesiredVelocity = 550,
                LateBrakeDistance = 70,
                OutOfCornerAccelerateDistance = 70
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 27.28,
                DesiredVelocity = 550,
                LateBrakeDistance = 70,
                OutOfCornerAccelerateDistance = 70
            });
            return bendProperties;
        }


        public static List<BendProperties> AddBendsNoInOut()
        {
            List<BendProperties> bendProperties = new List<BendProperties>();
            bendProperties.Add(new BendProperties()
            {
                Steepness = 143.24,
                DesiredVelocity = 275,
                LateBrakeDistance = 0,
                OutOfCornerAccelerateDistance = 0
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 95.49,
                DesiredVelocity = 325,
                LateBrakeDistance = 0,
                OutOfCornerAccelerateDistance = 0
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 63.66,
                DesiredVelocity = 390,
                LateBrakeDistance = 0,
                OutOfCornerAccelerateDistance = 0
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 52.09,
                DesiredVelocity = 420,
                LateBrakeDistance = 0,
                OutOfCornerAccelerateDistance = 0
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 30.16,
                DesiredVelocity = 550,
                LateBrakeDistance = 0,
                OutOfCornerAccelerateDistance = 0
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 27.28,
                DesiredVelocity = 550,
                LateBrakeDistance = 0,
                OutOfCornerAccelerateDistance = 0
            });
            return bendProperties;
        }


        public static List<BendProperties> AddBendsSlow()
        {
            List<BendProperties> bendProperties = new List<BendProperties>();
            bendProperties.Add(new BendProperties()
            {
                Steepness = 143.24,
                DesiredVelocity = 250,
                LateBrakeDistance = 20.94387043,
                OutOfCornerAccelerateDistance = 41.88774085
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 95.49,
                DesiredVelocity = 310,
                LateBrakeDistance = 31.41690229,
                OutOfCornerAccelerateDistance = 62.83380459
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 63.66,
                DesiredVelocity = 370,
                LateBrakeDistance = 47.12535344,
                OutOfCornerAccelerateDistance = 94.25070688
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 52.09,
                DesiredVelocity = 410,
                LateBrakeDistance = 50,
                OutOfCornerAccelerateDistance = 100
            });
            bendProperties.Add(new BendProperties()
            {
                Steepness = 26.04,
                DesiredVelocity = 500,
                LateBrakeDistance = 50,
                OutOfCornerAccelerateDistance = 100
            });

            return bendProperties;
        }
    }
}
