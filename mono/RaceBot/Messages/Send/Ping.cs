using System;

namespace RaceBot.Messages.Send
{
    [Obsolete("Sending Ping will probably harm the bot's performance. Just send new Throttle(1.0) if in doubt.")]
    public class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }
}