﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using RaceBot.Utilities;

namespace RaceBot.Tests
{
    [TestFixture]
    public class FcTransferDataTest
    {
        [Test]
        public void ExactF500R60()
        {
            var fcData = new CentrifugalForceTransferData();

            var value = fcData.PredictAlpha(60, 500);

            Assert.IsTrue(Math.Abs(0.310912023 - value) < 0.00001);
        }


        [Test]
        public void ExactF900R110()
        {
            var fcData = new CentrifugalForceTransferData();

            var value = fcData.PredictAlpha(110, 900);

            Assert.IsTrue(Math.Abs(0.794924906 - value) < 0.00001);
        }


        [Test]
        public void InterpolateF475R60()
        {
            var fcData = new CentrifugalForceTransferData();

            var value = fcData.PredictAlpha(60, 475);

            Assert.IsTrue(Math.Abs((0.277862754 + 0.310912023) / 2 - value) < 0.00001);
        }

        [Test]
        public void InterpolateF1000R60()
        {
            var fcData = new CentrifugalForceTransferData();

            var value = fcData.PredictAlpha(60, 1000);

            Assert.IsTrue(Math.Abs(0.587090951 + (0.587090951 - 0.551542884) * 2 - value) < 0.00001);
        }

        [Test]
        public void InterpolateF200R85()
        {
            var fcData = new CentrifugalForceTransferData();

            var value = fcData.PredictAlpha(85, 200);

            Assert.IsTrue(Math.Abs((0.160684301 + 0.118673221) / 2 - value) < 0.00001);
        }
    }
}
