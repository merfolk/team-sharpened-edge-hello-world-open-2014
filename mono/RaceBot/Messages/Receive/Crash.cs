﻿using System;

namespace RaceBot.Messages.Receive
{
    /// <summary>
    /// Represents the "crash" message sent by server.
    /// </summary>
    public class Crash
    {
        public string name;
        public string color;

        public override string ToString ()
        {
            return String.Format ("{0} crashed!", this.name);
        }
    }
}

