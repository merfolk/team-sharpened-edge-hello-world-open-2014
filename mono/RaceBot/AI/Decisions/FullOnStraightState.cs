﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SEdge.Common;

namespace RaceBot.AI.Decisions
{
    class FullOnStraightState : BaseDecision
    {
        public override bool PassThrough
        {
            get { return false; }
        }

        public FullOnStraightState(RaceKnowledge knowledge)
            : base(knowledge)
        {
        }

        public override void CalculateDecisionValue()
        {
            Value = 1;
        }

        public override void Execute(IZTime zTime)
        {
            knowledge.Throttle = 1;
            Log(zTime, "FSpd");
        }
    }
}
