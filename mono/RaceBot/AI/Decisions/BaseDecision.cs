﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SEdge.AI;
using SEdge.Common;
using RaceBot.Race;
using RaceBot.Utilities;

namespace RaceBot.AI.Decisions
{
    public abstract class BaseDecision : IAiState
    {
        protected readonly RaceKnowledge knowledge;
        private bool writePredictAngleData = false;
        private bool lastWasBend;
        //private bool lastWasBend = true;
        protected RaceTrack Track
        {
            get
            {
                return this.knowledge.Track;
            }
        }


        protected BaseDecision(RaceKnowledge knowledge)
        {
            this.knowledge = knowledge;
        }

        /// <summary>
        /// Returns a reference to our own car. Can be used to avoid writing knowledge.MyCar all over again.
        /// </summary>
        protected RaceCar MyCar { get { return this.knowledge.MyCar; } }

        public abstract bool PassThrough { get; }

        public double Value { get; protected set; }
        /// <summary>
        /// Positive values make this decision active
        /// </summary>
        public abstract void CalculateDecisionValue();
        public abstract void Execute(IZTime zTime);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="zTime">time</param>
        /// <param name="name">4 letter representation of this</param>
        /// <param name="additionalInfo"></param>
        protected void Log(IZTime zTime, string name, string additionalInfo = "")
        {
            if (knowledge.Logger.LogMode == LogMode.None)
            {
                knowledge.Logger.LogText = null;
                return;
            }

            var currentLane = MyCar.PiecePosition.lane;
            //var distance = currentLane.DistanceFromCenter;
            int bendSteepness = (int)MyCar.CurrentTrackPiece.GetBendSteepness(currentLane);
            string turboAvailability = (MyCar.IsTurboAvailable ? "T" : " ");

            int centrifugalForce = 0;
            if (MyCar.CurrentTrackPiece.IsBend())
                centrifugalForce = (int)RaceMath.CentrifugalForce(MyCar.CurrentVelocity, MyCar.CurrentTrackPiece.GetRadius(MyCar.PiecePosition.lane));

            if (knowledge.Logger.LogMode == LogMode.FcData)
            {
                if (!MyCar.IsSwitchingLanes() && !(MyCar.CurrentTrackPiece.GetPrevious().HasSwitch && MyCar.PiecePosition.inPieceDistance < 30))
                {
                    var alpha = Math.Abs(MyCar.TickAngleChangeSpeed + MyCar.AngleHarmonizer);
                    knowledge.Logger.LogText = "new ForceTransferData() {Alpha = " + alpha.ToString().Replace(',', '.') + ", F = " + (MyCar.LastFc - RaceMath.Fk).ToString().Replace(',', '.') +
                                               ", Radius = " + MyCar.LastRadius + "},";
                    knowledge.Logger.Log();
                    NormalLog(zTime, name, additionalInfo);
                }
                else
                {
                    knowledge.Logger.LogText = "switching probably";
                    knowledge.Logger.Log();
                    NormalLog(zTime, name, additionalInfo);
                }
                
            }
            else if (false)
            {
                //if (!lastWasBend && MyCar.Angle != 0)
                //{
                    


                //}

                //lastWasBend = MyCar.CurrentTrackPiece.IsBend();
            }
            else if (writePredictAngleData)
            {
                var predictedAngleSpeed = MyCar.TickAngleChange;
                var predictedNextAngle = MyCar.Angle;
                double predictedAngleAccel;
                double c = -0.007319;
                double k = -0.090382;
                for (int i = 0; i < 10; i++)
                {
                    predictedAngleAccel = predictedNextAngle * c + k * predictedAngleSpeed;
                    predictedAngleSpeed += predictedAngleAccel;
                    predictedNextAngle += predictedAngleSpeed;
                }

                knowledge.Logger.LogText = string.Format("{0,-4} {1, -4}: v: {2,-6} Th: {3,-4} A: {4,-5} I: {6,-2} C: {7, -3} PA: {10} ",
                zTime.Ticks, name, ZLib.FloatToText(MyCar.CurrentVelocity), ZLib.FloatToText(knowledge.Throttle),
                ZLib.DoubleToText(MyCar.Angle), ZLib.DoubleToText(MyCar.TickAngleChange), MyCar.PiecePosition.pieceIndex,
                    MyCar.CurrentTrackPiece.GetRadius(currentLane), turboAvailability, centrifugalForce,
                ZLib.DoubleToText(predictedNextAngle))
                         + additionalInfo;
            }
            else
            {
                 NormalLog(zTime,name, additionalInfo);
            }
            //knowledge.Logger.LogText +=" S "+ ZLib.FloatToText(knowledge.Cars[1].AiData.Slope);
            
        }


        private void NormalLog(IZTime zTime, string name, string additionalInfo)
        {
            int myPosition = knowledge.GetCurrentPosition(MyCar);

            if (MyCar.IsFinished)
            {
                knowledge.Logger.LogText = myPosition + ". " + knowledge.RaceTime.Ticks + " ...";
                return;
            }

            var currentLane = MyCar.CurrentLane;
            int bendSteepness = (int)MyCar.CurrentTrackPiece.GetBendSteepness(currentLane);
            string turboAvailability = (MyCar.IsTurboAvailable ? "T" : " ");

            int centrifugalForce = 0;
            if (MyCar.CurrentTrackPiece.IsBend())
                centrifugalForce = (int)RaceMath.CentrifugalForce(MyCar.CurrentVelocity, MyCar.CurrentTrackPiece.GetRadius(currentLane));

            knowledge.Logger.LogText = string.Format(
                "{0}. {1,-4} {2, -5} v {3,-6} h {4,-4} A {5,-6} I {7,-2} S {8, -3} F {10,-4} {9} t {11} ",
                myPosition, zTime.Ticks, name, ZLib.FloatToText(MyCar.CurrentVelocity), ZLib.FloatToText(knowledge.Throttle),
                ZLib.FloatToText(MyCar.Angle), ZLib.DoubleToText(MyCar.TickAngleChange),
                MyCar.PiecePosition.pieceIndex,
                bendSteepness, turboAvailability,
                centrifugalForce, knowledge.Logger.DecisionTimeTaken)
                                       + additionalInfo;
        }
    }
}
