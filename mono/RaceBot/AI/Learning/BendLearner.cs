﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RaceBot.Messages.Receive;
using RaceBot.Race;
using RaceBot.SEdge.Common;
using RaceBot.Utilities;

namespace RaceBot.AI.Learning
{
    [Serializable]
    public sealed class BendLearner
    {
        private const double longestStraightConnectingBend = 90;
        private const double safeMarginFromCrash = 0.25;
        private List<SingleBendAdjustment> bendAdjustments;
        private SingleBendAdjustment lastBend;


        private PiecePosition lastPosition = new PiecePosition();
        private double raceSectionMinAngle;
        private double raceSectionMaxAngle;

        private SingleBendAdjustment earlierBend;
        private double earlierAdjustment;

        /// <summary> If we have crashed on this lap, do not update new adjustment data except for new crashes </summary>
        private int invalidLap = -1;

        [NonSerialized]
        private readonly RaceLogger logger;

        public List<SingleBendAdjustment> BendAdjustments
        {
            get { return bendAdjustments; }
            set { bendAdjustments = value; }
        }
        /// <summary>
        /// Do not use, only for serializer to use
        /// </summary>
        public BendLearner()
        {
            this.logger = null;
        }


        public static BendLearner Create(bool EnableLearner, RaceTrack track, RaceLogger logger)
        {
            BendLearner bendLearner  = null;
#if DEBUG
            //var filename = track.Name + ".xml";
            //if (EnableLearner)
            //{
            //    if (File.Exists(filename))
            //    {

            //        bendLearner = (BendLearner)ZSerializer.ReadFile(filename, typeof(BendLearner));
            //    }
            //}
#endif
            if (bendLearner == null)
                bendLearner = new BendLearner(track, logger);
            return bendLearner;
        }

        public BendLearner(RaceTrack track, RaceLogger logger)
        {
            this.logger = logger;
            // ReSharper disable CompareOfFloatsByEqualityOperator
            var lane = new Lane() {startLaneIndex = 0, endLaneIndex = 0};
            bendAdjustments = new List<SingleBendAdjustment>();
            var position = new PiecePosition()
                           {
                               inPieceDistance = 0,
                               lane = new Lane() {startLaneIndex = 0, endLaneIndex = 0},
                               lap = 1,
                               pieceIndex = 0
                           };

            double lastSteepness = 0;
            for (int i = 0; i < track.Pieces.Count; i++)
            {
                var piece = track.CurrentTrackPiece(i);
                var bendSteepness = piece.GetBendSteepness(lane);
                if (bendAdjustments.Count > 0)
                    position.pieceIndex = bendAdjustments[bendAdjustments.Count - 1].BendEnd + 1;

                if (bendAdjustments.Count > 0 &&
                    Math.Sign(lastSteepness) == Math.Sign(bendSteepness) &&
                    track.DistanceToPiece(i, position) <= longestStraightConnectingBend)
                {
                    bendAdjustments[bendAdjustments.Count - 1].BendEnd = i;
                    bendAdjustments[bendAdjustments.Count - 1].MaxSteepness = Math.Max(Math.Abs(bendSteepness),
                        bendAdjustments[bendAdjustments.Count - 1].MaxSteepness);
                }
                else if (bendSteepness != 0 && 
                    (lastSteepness == 0 || Math.Sign(lastSteepness) != Math.Sign(bendSteepness) ||
                    (bendAdjustments.Count > 0 &&
                    track.DistanceToPiece(i, position) > longestStraightConnectingBend)))
                {
                    bendAdjustments.Add(new SingleBendAdjustment()
                                        {
                                            BendStart = i,
                                            BendEnd = i,
                                            MaxSteepness = Math.Abs(bendSteepness),
                                            IsLeft = (bendSteepness < 0)
                                        });
                    lastSteepness = bendSteepness;
                }
                else if (bendSteepness == 0 && bendAdjustments.Count > 0 &&
                         Math.Sign(lastSteepness) == Math.Sign(bendSteepness) &&
                         track.DistanceToPiece(i, position) > longestStraightConnectingBend)
                {
                    lastSteepness = 0;
                }
            }
        }


        public double GetBendAdjustment(int pieceIndex)
        {
            foreach (var adjustment in BendAdjustments)
            {
                if (adjustment.IndexMatch(pieceIndex))
                    return adjustment.Adjustment;
            }
            logger.WriteLine("No matching bend index!");
            return 0;
        }
        public SingleBendAdjustment GetBendAdjustmentPiece(int pieceIndex)
        {
            foreach (var adjustment in BendAdjustments)
            {
                if (adjustment.IndexMatch(pieceIndex))
                    return adjustment;
            }
            logger.WriteLine("No matching bend index!");
            return null;
        }

        public void UpdateCrash()
        {
            if (lastBend == null) return;

            //RaceMath.Fk -= 5;
            //Console.WriteLine("Adjusting Fk: -5: " + RaceMath.Fk);
            //lastBend.LowestCrash = Math.Min(lastBend.LowestCrash, lastBend.Adjustment);
            //if (lastBend.SafeBottom >= lastBend.LowestCrash - 2)
            //    lastBend.SafeBottom = lastBend.LowestCrash - 2;
            //var adj = (lastBend.SafeBottom + lastBend.Adjustment) * 0.5f;

            lastBend.Adjustment += -10;
            logger.WriteLine("New bend adjustment: " + lastBend.Adjustment + " BendStart: " + lastBend.BendStart);

            //invalidLap = lastPosition.lap;
            lastBend = null;
            earlierBend = null;
        }


        public void Update(double carAngle, PiecePosition position)
        {
            // TODO: check whether this is sufficient?
            if (lastBend == null ||
                (!lastBend.IndexMatch(position.pieceIndex) &&
                 (position.lap > lastPosition.lap || position.pieceIndex > lastPosition.pieceIndex + 1 ||
                  (position.pieceIndex > lastPosition.pieceIndex && position.inPieceDistance > 50))))
            {
                // position has changed
                foreach (var adjustment in BendAdjustments)
                {
                    if (adjustment.IndexMatch(position.pieceIndex))
                    {
                        // only update new data if we have not crashed yet
                        if (lastBend != null && invalidLap != lastPosition.lap)
                        {
                            UpdateAdjustment(lastBend);
                        }

                        earlierBend = lastBend;
                        lastBend = adjustment;
                        raceSectionMinAngle = carAngle;
                        raceSectionMaxAngle = carAngle;
                    }
                }
            }

            // copy up to until the last position inside that last bend
            if (lastBend != null && lastBend.IndexMatch(position.pieceIndex))
            {
                // Copy parameters (cannot copy object, because it would only copy the reference)
                lastPosition.inPieceDistance = position.inPieceDistance;
                lastPosition.pieceIndex = position.pieceIndex;
                lastPosition.lane = position.lane;
                lastPosition.lap = position.lap;
            }


            raceSectionMinAngle = Math.Min(carAngle, raceSectionMinAngle);
            raceSectionMaxAngle = Math.Max(carAngle, raceSectionMaxAngle);
        }


        private void UpdateAdjustment(SingleBendAdjustment adjustment)
        {
            return; // no learning
            if (lastPosition.lap == 1 && lastPosition.pieceIndex < 6)
                return;

            double angleBoundary;
            if (adjustment.IsLeft)
            {
                angleBoundary = -raceSectionMinAngle;
            }
            else
            {
                angleBoundary = raceSectionMaxAngle;
            }

            adjustment.LastMaxAngle = angleBoundary;

            var targetAngle = 59;
            var safeAngle = targetAngle - 2;

            if (angleBoundary < safeAngle)
            {
                lastBend.SafeBottom = lastBend.Adjustment;
                if (adjustment.SafeBottom > adjustment.LowestCrash - safeMarginFromCrash * 2)
                    adjustment.LowestCrash = adjustment.SafeBottom + safeMarginFromCrash;
            }
            //if (angleBoundary < targetAngle)
            //{
            double adj = adjustment.Adjustment;
            // back up
            //if (angleBoundary < targetAngle)
            //    adj = adjustment.Adjustment + (targetAngle - angleBoundary) * (targetAngle - angleBoundary) * 0.01;
            if (angleBoundary < targetAngle)
                adj = adjustment.Adjustment + (targetAngle - angleBoundary) * 1;
            
            if (adj >= adjustment.LowestCrash - safeMarginFromCrash)
            {
                if (adjustment.Adjustment >= adjustment.LowestCrash - safeMarginFromCrash * 2)
                    adj = adjustment.LowestCrash - safeMarginFromCrash;
                else
                    adj = (adjustment.Adjustment + adjustment.LowestCrash - safeMarginFromCrash) / 2;
            }
            //else if (adj < adjustment.Adjustment)
            //    adj = adjustment.Adjustment;

            if (earlierBend != null && earlierBend.BendEnd <= adjustment.BendStart - 2 && earlierAdjustment > 0 &&
                earlierBend.MaxSteepness > 50)
            {
                adj = Math.Min(adjustment.Adjustment, adj);
                adj -= earlierAdjustment ;
                adjustment.SafeBottom -= earlierAdjustment; // we can no longer be sure that our safe bottom is truly safe
            }
            //else if (earlierBend != null && earlierBend.BendEnd <= adjustment.BendStart - 2 && earlierAdjustment < 0 &&
            //    earlierBend.MaxSteepness > 50)
            //{
            //    adj = Math.Min(adjustment.Adjustment, adj);
            //    adjustment.LowestCrash = earlierAdjustment; // perhaps the crash is no longer optimal
            //}

            if (adj != adjustment.Adjustment)
                Console.WriteLine("Adjusting Corner: " + (adj - adjustment.Adjustment));
            adjustment.Adjustment = Math.Min(adj, 100);
            earlierAdjustment = adj - adjustment.Adjustment;
        }


        //}
    }
}
