﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Utilities;
using SEdge.Common;
using RaceBot.Race;

namespace RaceBot.AI.Decisions
{
    public class CorneringStatePrime : BaseDecision
    {
        protected double desiredVelocity = 370;
        protected double lastSteepness = 0;
        protected double lateBrakeDistance;
        protected string logName;
        protected double bendForce;


        public CorneringStatePrime(RaceKnowledge knowledge, double bendForce) : base(knowledge)
        {
            this.bendForce = bendForce;
            logName = "Crnp";
        }


        public override bool PassThrough
        {
            get { return false; }
        }


        public override void CalculateDecisionValue()
        {
			var myCarOnPiece = MyCar.PiecePosition.pieceIndex;
            RaceTrackPiece nextBend = Track.NextOrCurrentTurn (myCarOnPiece);

            var predictLane = knowledge.PredictLane(nextBend.Index);

            var steepness = nextBend.GetBendSteepness(predictLane);

            double distance = GetBrakeDistance(steepness, nextBend, myCarOnPiece);
            var velocity = MyCar.CurrentVelocity;
            int ticks;
            double brakingDistance = 0;
            for (ticks = 0; ticks < 50; ticks++)
            {
                brakingDistance += velocity * RaceMath.TickDuration;
                velocity = RaceMath.VelocityOnNextTick(velocity);
                // add one extra tick to calculation for super late braking optimized to subtick
                if (velocity <= desiredVelocity)
                    break;
            }

            
            //var brakingDistance = RaceMath.BreakingDistance(
            //    RaceMath.GetTickFromVelocity(MyCar.CurrentVelocity),
            //    RaceMath.GetTickFromVelocity(desiredVelocity)) - lateBrakeDistance;

            if (distance <= 0 || distance < brakingDistance - lateBrakeDistance)
            {
                Value = 1; // activate
            }
            else
                Value = -1; // deactivate
                
        }


        protected virtual double GetBrakeDistance(double steepness, RaceTrackPiece nextBend, int myCarOnPiece)
        {
            var distance = Track.DistanceToPiece(nextBend.Index, MyCar.PiecePosition);

            if (distance > 0 || steepness != lastSteepness)
            {
                lastSteepness = steepness;
                //steepness = Math.Abs(steepness);


                double fc = RaceMath.Fk + bendForce;
                int distanceFromCenter;
                var bend = nextBend;
                var totalAngle = 0.0;
                double totalStraight = 0;
                for (int i = 0; i < 10; i++)
                {
                    var predictLane = knowledge.PredictLane(bend.Index);

                    if (bend.GetBendSteepness(predictLane) == steepness)
                    {
                        totalAngle += bend.Angle;
                    }
                    else
                    {
                        //if (bend.IsStraight())
                        //{
                        //    totalStraight += bend.GetLength(0);
                        //}
                        //if (totalStraight > 90 || (bend.IsBend() && lastSteepness != bend.GetBendSteepness(distanceFromCenter)))
                            break;
                    }
                    bend = bend.GetNext();
                }
                double angleMultiplier = Math.Pow(Math.Min(90 / totalAngle, 1),3);

                fc += Math.Max(angleMultiplier * 500 - MyCar.Angle * Math.Sign(steepness) * 2, 50);
                //fc += Math.Max((180 - totalAngle) * 5 - MyCar.Angle * Math.Sign(steepness) * 1, 0);
                //fc += Math.Max((180 - totalAngle) * 2.85, 0);
                if (knowledge.EnableLearner)
                    fc += knowledge.BendLearner.GetBendAdjustment(nextBend.Index);

                desiredVelocity = Math.Sqrt(fc * nextBend.GetRadius(MyCar.PiecePosition.lane));
                lateBrakeDistance = knowledge.BendsData.GetBendValues(steepness).LateBrakeDistance;
          
                if (desiredVelocity < 1) desiredVelocity = 1;



            }
            return distance;
        }


        protected double PredictBrakingDistance(out double velocity)
        {
            double brakingDistance = 0;
            velocity = MyCar.CurrentVelocity;
            int ticks;
            for (ticks = 0; ticks < 50; ticks++)
            {
                brakingDistance += velocity * RaceMath.TickDuration;
                velocity = RaceMath.VelocityOnNextTick(velocity);
                // add one extra tick to calculation for super late braking optimized to subtick
                if (velocity <= desiredVelocity)
                    break;
            }
            return brakingDistance;
        }

        public override void Execute(IZTime zTime)
        {
            double desiredSpeedDelta;
            
            desiredSpeedDelta = desiredVelocity - MyCar.CurrentVelocity;
            

            desiredSpeedDelta -= // remove drag from air resistance
                RaceMath.VelocityOnNextTick(MyCar.CurrentVelocity) -
                MyCar.CurrentVelocity;

            // Slightly reduces desired speed based on the current car angle
            //var throttleEase = Math.Cos(MyCar.Angle / 180 * Math.PI);
            knowledge.Throttle = Math.Min(Math.Max(desiredSpeedDelta / MyCar.Acceleration, 0), 1);

            Log(zTime, logName);
        }
    }
}
