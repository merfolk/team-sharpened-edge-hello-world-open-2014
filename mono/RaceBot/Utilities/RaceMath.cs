﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaceBot.Utilities
{
    public static class RaceMath
    {
        public const double e = 2.71828182845904523536028747135266249775724709369995;

        public const double TickDuration = 1d / 60d;
        [Obsolete]
        public const double multiplier = 551.6926575;
        [Obsolete]
        public const double exponent = -0.0202028;

        public const double CrashAngle = 59.9;

        public static double Acceleration = 12;

        public static double Fk = 900;//-100; // start safe
        public static bool FkFound = false;
        public const double c = -0.1;
        public const double k = -0.000020833333333;
        public const double Fcm = 0.000681711;

        private const double magic = 0.0025;

        private static CentrifugalForceTransferData _fcData = new CentrifugalForceTransferData();
        
        /// <summary>
        /// Air resistance speed reduction multiplier
        /// </summary>
        public static double Arsx = 1.2;

        [Obsolete]
        public static double PredictSpeedOnTick(double tick)
        {
            
            return multiplier * Math.Pow(e, exponent * tick);
        }


        public static double VelocityOnNextTick(double velocity)
        {
            return velocity - velocity * Arsx * TickDuration;
        }


        public static double GetRadius(double carAngleAlpha,double carAngle, double carAngleDelta, double carVelocity, double radius)
        {
            var y = carAngleDelta * c + carAngle * k * carVelocity;
            carAngleAlpha -= y + carAngleDelta;
            
            carAngleAlpha = Math.Abs(carAngleAlpha);
            var rStart = 1.0;
            var rEnd = 1000.0;
            if (carAngleAlpha < 0.000001)
                return 0;
            double Fc;
            while (Math.Abs(rStart - rEnd) > 0.0001)
            {
                var r = (rStart + rEnd) / 2;
                Fc = CentrifugalForce(carVelocity, r);
                var F = Fc - (Fk);
                var alphaFc = Magic(F, carVelocity, r);
                if (alphaFc < carAngleAlpha)
                    rEnd = r;
                else 
                {
                    rStart = r;
                }
            }
            Fc = CentrifugalForce(carVelocity, radius);
            var alpaa = Magic(Fc - (Fk), carVelocity, radius);
            return (rStart + rEnd) / 2; ;
        }

        public static double AngleAccelOnNextTick(double carAngle, double carAngleDelta, double carVelocity, double pieceRadius, bool isLeftBend, double FkAdjustment = 0)
        {
            double a = 0;
            var Fc = CentrifugalForce(carVelocity, pieceRadius);
            var Fo = carAngleDelta * c + carAngle * k * carVelocity;
            //Fo = hData.Find(carAngle, carAngleDelta);

            var F = Fc - (Fk + FkAdjustment);
            double alphaFc = 0;
            if (F > 0)
            {
                //alphaFc = _fcData.PredictAlpha(pieceRadius, F);
                //alphaFc = _fcData.PredictAlpha(40, F) / Math.Sqrt(40) * Math.Sqrt(pieceRadius);
                //alphaFc = Math.Pow(F, 1.105) * Math.Pow(pieceRadius, 0.5) * MagicValueA;
                alphaFc = Magic(F, carVelocity, pieceRadius);
                //alphaFc = ([@F]*[@r]+[@vd]*[@vd])/[@vk]
            }
            a = Fo;

            if (pieceRadius > 0)
            {
                if (isLeftBend)
                    a += Math.Min(-alphaFc, 0);
                else
                    a += Math.Max(alphaFc, 0);
            }
            return a;
        }


        public static double HarmonicTenderizer(double carAngle, double carAngleDelta, double carVelocity)
        {
            return carAngleDelta * c + carAngle * k * carVelocity;
        }

        public static double Magic(double F, double oldCarVelocity, double pieceRadius)
        { 
            var vk = Math.Sqrt(Fk * pieceRadius);
            var vd = oldCarVelocity - vk;
            return magic * (F * pieceRadius + vd * vd) / vk;
        }

        public static double CalculateFkByAngle(double carAngleAlpha, double oldCarAngle, double oldcarAngleDelta, double oldCarVelocity,
            double pieceRadius, bool isLeftBend)
        {
            var y = oldcarAngleDelta * c + oldCarAngle * k * oldCarVelocity;
            carAngleAlpha -= y + oldcarAngleDelta;
            
            carAngleAlpha = Math.Abs(carAngleAlpha);

            var Fc = CentrifugalForce(oldCarVelocity, pieceRadius);
            var m2 = magic * magic;


            var F = m2 * Math.Pow(Fc * pieceRadius + oldCarVelocity * oldCarVelocity, 2) /
                    (pieceRadius * Math.Pow(carAngleAlpha + 2 * magic * oldCarVelocity, 2));

            
            if (double.IsNaN(F))
                return 0;
            
            if (Fc < Fk)
                return 0;
            return F;
        }

        [Obsolete]
        public static double GetTickFromVelocity(double velocity)
        {
            return Math.Log(velocity / multiplier, e) / exponent;
        }

        /// <summary>
        /// calculates centrifugal force according to equation: Fc = m * v^2 / r
        /// </summary>
        /// <param name="velocity">Current velocity.</param>
        /// <param name="radius">Radius of the bend. Remember to take current lane into account!</param>
        /// <param name="mass">Mass of the car. Default is 1 "kg".</param>
        public static double CentrifugalForce(double velocity, double radius)
        {
            return velocity * velocity / radius;
        }

        public static double BreakingDistance(double currentVelocityTick, double desiredVelocityTick)
        {
            // integral from current to desired
            return PredictSpeedOnTick(currentVelocityTick) - PredictSpeedOnTick(desiredVelocityTick);
        }

        /// <summary>
        /// Calculates the arc length of a given circular sector.
        /// </summary>
        /// <param name="angle">Angle of the circular sector.</param>
        /// <param name="radius">Radius of the circular sector.</param>
        public static double CircularSectorArcLength(double angle, double radius)
        {
            return (angle / 360) * 2 * Math.PI * radius;
        }

        /// <summary>
        /// Calculates the slope of a linear function fitted to the data using linear regression.
        /// Make sure the data does not contain values that are way off, because this function does
        /// nothing to exclude those!
        /// </summary>
        /// <remarks>>
        /// For the equation used see http://en.wikipedia.org/wiki/Simple_linear_regression#Numerical_example
        /// </remarks>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        public static double SlopeOfLinearRegression(double[] x, double[] y)
        {
            if (x.Length == 0)
                throw new ArgumentException("x parameter is empty.");

            if (y.Length == 0)
                throw new ArgumentException("y parameter is empty.");

            if (x.Length != y.Length)
                throw new ArgumentException("Argument array sizes need to be of the same size.");

            int n = x.Length;

            double Sx = x.Sum();
            double Sy = y.Sum();

            double Sxx = 0;

            for(int i = 0; i < n; i++)
            {
                Sxx += Math.Pow(x[i], 2);
            }

            double Sxy = 0;

            for (int i = 0; i < n; i++)
            {
                Sxy += (x[i] * y[i]);
            }

//            Console.WriteLine("n: " + n);
//            Console.WriteLine("Sx: " + Sx);
//            Console.WriteLine("Sy: " + Sy);
//            Console.WriteLine("Sxx: " + Sxx);
//            Console.WriteLine("Sxy: " + Sxy);
//            Console.WriteLine();

            double dividend = n * Sxy - Sx * Sy;
            double divisor = n * Sxx - Math.Pow(Sx, 2);

//            Console.WriteLine("dividend: " + dividend);
//            Console.WriteLine("divisor: " + divisor);

            double slope = dividend / divisor;

            return slope;
        }
    }
}
