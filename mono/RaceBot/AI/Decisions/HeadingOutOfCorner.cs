﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Messages.Receive;
using RaceBot.Utilities;
using SEdge.Common;

namespace RaceBot.AI.Decisions
{
    [Obsolete("Will get to an infinite loop on a track with no straight pieces. You have been warned!")]
    public class HeadingOutOfCornerState : BaseDecision
    {
        public bool BewareGettingOutOfControl = true;
        private double endOfTurnDistance;

        //private const int outSideCornerDistance = 70+33;
        //private const int insideCornerDistance = 50+32;
        //private const int insidesteepCornerDistance = 70;
        //private const int outSidesteepCornerDistance = 60;
        private double steepness;

        public override bool PassThrough
        {
            get { return false; }
        }

        public HeadingOutOfCornerState(RaceKnowledge knowledge) : base(knowledge)
        {

        }


        public override void CalculateDecisionValue()
        {
            var currentPiece = MyCar.CurrentTrackPiece;
            if(!currentPiece.IsBend())
            {
                Value = -1;
                return;
            }

            var nextPiece = MyCar.CurrentTrackPiece.GetNext();

            var predictLane = knowledge.PredictLane(nextPiece.Index);
            endOfTurnDistance = nextPiece.GetLength(predictLane) - MyCar.PiecePosition.inPieceDistance;

            while (Math.Abs(currentPiece.GetBendSteepness(predictLane) -
                nextPiece.GetBendSteepness(predictLane)) < 0.000001)
            {
                endOfTurnDistance += nextPiece.GetLength(predictLane);
                nextPiece = nextPiece.GetNext ();
            }

            if (Track.DistanceToCurrentOrNextBend(currentPiece.Index, MyCar.PiecePosition.inPieceDistance) > 0 || 
                !currentPiece.IsBend())
            {
                // not in a turn currently
                Value = -1;
                return;
            }

            steepness = currentPiece.GetBendSteepness(predictLane);

            if(endOfTurnDistance < knowledge.BendsData.GetBendValues(Math.Abs(steepness)).OutOfCornerAccelerateDistance)
            {
                // Let's simulate upcoming car angles until the end of turn

                Value = 1;
            }
            else
            {
                Value = -1;
            }
        }


        


        public override void Execute(IZTime zTime)
        {
            double timeUntilBendEnd = endOfTurnDistance / MyCar.CurrentVelocity;
            bool scaryAngle =Math.Abs(MyCar.Angle + MyCar.AngleSpeed * timeUntilBendEnd) > 60;

            if (BewareGettingOutOfControl && scaryAngle)
            {
                knowledge.Throttle = 0.5f;
            }
            else
            {
                knowledge.Throttle = 1f;
            }
            Log(zTime, "OCrn");
        }
    }
}
