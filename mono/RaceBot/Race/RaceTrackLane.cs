﻿namespace RaceBot.Race
{
    public class RaceTrackLane
    {
        public int Index { get; private set; }

        /// <summary>
        /// A positive value tells that the lanes is to the right from the center line while a negative value indicates a lane to the left from the center.
        /// </summary>
        /// <value>The distance from center.</value>
        public int DistanceFromCenter { get; private set; }

        public RaceTrackLane(int index, int distanceFromCenter)
        {
            this.Index = index;
            this.DistanceFromCenter = distanceFromCenter;
        }
    }
}