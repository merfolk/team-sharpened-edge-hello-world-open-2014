﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SEdge.AI;
using SEdge.Common;
using RaceBot.Messages.Receive;

namespace RaceBot.AI
{
    public class RacingAI
    {
        private readonly IAiState ai;
        private readonly RaceKnowledge knowledge;
        public IAiState ExecuteLast;

        public RacingAI(IAiState ai, RaceKnowledge knowledge)
        {
            this.knowledge = knowledge;
            this.ai = ai;
        }


        public RaceKnowledge Knowledge
        {
            get { return knowledge; }
        }


        public void Execute(IZTime zTime, CarPosition[] carPositions)
        {
            Knowledge.Update(zTime, carPositions);
            ai.CalculateDecisionValue();
            ai.Execute(zTime);
            if (ExecuteLast != null)
            {
                ExecuteLast.CalculateDecisionValue();
                if (ExecuteLast.Value > 0)
                    ExecuteLast.Execute(zTime);
            }
            knowledge.Logger.Log();
        }
    }
}
