﻿using System;
using RaceBot.AI.Decisions;
using RaceBot.Utilities;
using SEdge.AI;
using SEdge.Common;

namespace RaceBot.AI
{
    [Obsolete("use Spin preventer prime instead")]
    public class SpinPreventer : BaseDecision
    {
        private double lastAngleSpeed = 0;
        private int tickCountPreventer;
        double endOfTurnDistance;

        public SpinPreventer(RaceKnowledge knowledge, int tickCountPreventer) : base(knowledge)
        {
            this.tickCountPreventer = tickCountPreventer;
        }


        public override bool PassThrough
        {
            get { return false; }
        }


        public override void CalculateDecisionValue()
        {
            var currentPiece = MyCar.CurrentTrackPiece;
            var nextPiece = currentPiece.GetNext();

            
            endOfTurnDistance = nextPiece.GetLength(MyCar.CurrentLane) - MyCar.PiecePosition.inPieceDistance;

            //var speedDelta = (lastAngleSpeed - MyCar.AngleSpeed) * RaceMath.TickDuration;
            if (WillCrashOnFullSpeed(endOfTurnDistance) && currentPiece.IsBend())
                Value = 1; // only activate in corners
            else
                Value = -1;

            lastAngleSpeed = MyCar.AngleSpeed;
        }

        private bool WillCrashOnFullSpeed(double endOfTurnDistance)
        {
            var angle = MyCar.Angle;
            var tickChange = MyCar.TickAngleChange;
            var v = MyCar.CurrentVelocity;
            for (int i = 0; i < tickCountPreventer; i++)
            {
                if (endOfTurnDistance < 0)
                {
                    tickChange -= 0.55;
                }
                endOfTurnDistance -= v * RaceMath.TickDuration;
                angle += tickChange;
                v = RaceMath.VelocityOnNextTick(v) + MyCar.Acceleration;
                if (angle < -RaceMath.CrashAngle || angle > RaceMath.CrashAngle)
                    return true;

                if (Math.Sign(angle) != Math.Sign(tickChange))
                    return false;
            }

            return false;
        }

        public override void Execute(IZTime zTime)
        {
            knowledge.Throttle = Math.Max(Math.Min((endOfTurnDistance + 50) / 200, 1), 0);
            //if (endOfTurnDistance > 0)
            //    knowledge.Throttle = 0.75;
            //else
            //{
            //    knowledge.Throttle = 0;
            //}
            Log(zTime, "SpnP");
        }
    }
}