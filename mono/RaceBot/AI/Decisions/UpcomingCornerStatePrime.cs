﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Race;
using RaceBot.Utilities;

namespace RaceBot.AI.Decisions
{
    public class UpcomingCornerStatePrime : CorneringStatePrime
    {
        public UpcomingCornerStatePrime(RaceKnowledge knowledge, double fc) : base(knowledge, fc)
        {
            logName = "UCSP";
        }

        public override void CalculateDecisionValue()
        {
            Value = -1; // deactivate
            var myCarOnPiece = MyCar.PiecePosition.pieceIndex;
            var nextBend = Track.NextOrCurrentTurn(myCarOnPiece);
            for (int i = 0; i < 4; i++)
            {

                RaceTrackPiece secondBend = nextBend;

                var predictLane = knowledge.PredictLane(secondBend.Index);

                var previousSteepness = nextBend.GetBendSteepness(predictLane);
                int index = 0;
                while (Math.Abs(previousSteepness -
                    secondBend.GetBendSteepness(predictLane)) < 0.0001 ||
                    secondBend.IsStraight())
                {
                    index++;
                    if (index >= knowledge.Track.Pieces.Count)
                        return; // no upcoming corners

                    secondBend = secondBend.GetNext();
                    if (secondBend.IsStraight())
                        previousSteepness = 0;
                }

                predictLane = knowledge.PredictLane(secondBend.Index);
                var steepness = secondBend.GetBendSteepness(predictLane);
                //if (Math.Abs(steepness) >
                //    Math.Abs(secondBend.GetBendSteepness(distanceFromCenter)))
                //{
                //    Value = -1;
                //    return;
                //}
                //desiredVelocity = Math.Sqrt(fc * secondBend.GetRadius(MyCar.PiecePosition.lane.endLaneIndex));
                double distance = GetBrakeDistance(steepness, secondBend, myCarOnPiece);


                double velocity;
                var brakingDistance = PredictBrakingDistance(out velocity);

                if (distance < brakingDistance + velocity * RaceMath.TickDuration &&
                    MyCar.CurrentVelocity >= desiredVelocity)
                {
                    //Console.WriteLine("Prepare second corner steepness: " + ZLib.FloatToText(steepness));
                    Value = 1; // activate
                    return;
                }
                nextBend = secondBend;
            }   

        }

    }
}
