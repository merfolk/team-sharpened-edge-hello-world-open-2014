using System;
using Newtonsoft.Json;

namespace RaceBot.Messages.Send
{
    public abstract class SendMsg
    {
        public long gameTick;

        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData(), this.gameTick));
        }
        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }
}