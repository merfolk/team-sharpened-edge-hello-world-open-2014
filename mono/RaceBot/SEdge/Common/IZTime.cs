using System;

namespace SEdge.Common
{
    public interface IZTime
    {
        TimeSpan ElapsedTime { get; set; }
        TimeSpan TotalTime { get; set; }
        long Ticks { get; }

        /// <summary> Gets elapsed game time as float</summary>
        float F { get; }

        /// <summary> Gets elapsed game time as double</summary>
        double D { get; }

        /// <summary> Gets total game time as float</summary>
        float TotalF { get; }

        /// <summary> Gets total game time as double</summary>
        double TotalD { get; }
    }
}