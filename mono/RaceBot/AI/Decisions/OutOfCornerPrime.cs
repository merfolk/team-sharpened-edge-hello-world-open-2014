﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Messages.Receive;
using RaceBot.Utilities;
using SEdge.Common;

namespace RaceBot.AI.Decisions
{
    public class OutOfCornerPrime : SpinPreventerPrime
    {
        

        //private const int outSideCornerDistance = 70+33;
        //private const int insideCornerDistance = 50+32;
        //private const int insidesteepCornerDistance = 70;
        //private const int outSidesteepCornerDistance = 60;
        private double steepness;

        public override bool PassThrough
        {
            get { return false; }
        }

        public OutOfCornerPrime(RaceKnowledge knowledge) : base(knowledge, 50)
        {

        }


        public override void CalculateDecisionValue()
        {
            var currentPiece = MyCar.CurrentTrackPiece;
            if(!currentPiece.IsBend())
            {
                Value = -1;
                return;
            }

            double endOfTurnDistance;

            var nextPiece = MyCar.CurrentTrackPiece.GetNext();
            var predictLane = knowledge.PredictLane(nextPiece.Index);

            endOfTurnDistance = nextPiece.GetLength(predictLane) - MyCar.PiecePosition.inPieceDistance;
            for (int i = 0; i < Track.Pieces.Count; i++)
            {
                endOfTurnDistance += nextPiece.GetLength(predictLane);
                nextPiece = nextPiece.GetNext();
                if (!(Math.Abs(currentPiece.GetBendSteepness(predictLane) -
                               nextPiece.GetBendSteepness(predictLane)) < 0.000001))
                    break;
                predictLane = knowledge.PredictLane(nextPiece.Index);
            }

            if (Track.DistanceToCurrentOrNextBend(currentPiece.Index, MyCar.PiecePosition.inPieceDistance) > 0 || 
                !currentPiece.IsBend())
            {
                // not in a turn currently
                Value = -1;
                return;
            }



            steepness = currentPiece.GetBendSteepness(predictLane);
            Throttle = 1;
            if(!WillCrashOnSpeed())
            {
                // Let's simulate upcoming car angles until the end of turn
                Value = 1;
            }
            else
            {
                Value = -1;
            }
        }


        


        public override void Execute(IZTime zTime)
        {
            knowledge.Throttle = 1f;

            Log(zTime, "OCrp", " " + ZLib.FloatToText(maxAngle));
        }
    }
}
