﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Messages.Send;
using RaceBot.Race;
using RaceBot.Utilities;
using SEdge.Common;

namespace RaceBot.AI.Decisions
{
    public class TurboBouncerState : BaseDecision
    {
        public RaceCar Activated;
        protected bool ensureKill = false;
        protected double previousDistance;
        protected SwitchDirection switchTo = SwitchDirection.None;
        protected int switchingLaneFor = -1;
        protected double smallestDistance = 999;
        protected RaceCar closest;
        protected double myLastSpeed;
        public TurboBouncerState(RaceKnowledge knowledge)
            : base(knowledge)
        {
        }


        public override bool PassThrough
        {
            get { return false; }
        }

        public override void CalculateDecisionValue()
        {
            if (knowledge.IsQualifyingPeriod || knowledge.MyCar.OverallRanking < 2)
            {
                Value = -1;
                return;
            }

            knowledge.IsBouncing = false;
            Value = -1;

            var nextSwitch = Track.NextSwitch(MyCar.PiecePosition.pieceIndex);

            if (nextSwitch != null && switchingLaneFor != nextSwitch.Index)
                switchingLaneFor = -1;

            if (Activated == null || Activated.AiData.LastDistanceFromMyCar < 45 || Activated.IsOut ||
                MyCar.IsOut || Activated.AiData.LastDistanceFromMyCar > 200 ||
                myLastSpeed > MyCar.CurrentVelocity + 50)
            {
                if (Activated != null)
                {
                    if (Activated.IsOut || !ensureKill || MyCar.IsOut)
                    {
                        Activated = null;
                    }
                    else
                    {
                        Activate();
                    }
                    return;
                }
            }
            else
            {
                Activate();
                return;
            }

            if (MyCar.CurrentTrackPiece.IsBend())
                return;

            if (!knowledge.MyCar.IsTurboActive && !knowledge.MyCar.IsTurboAvailable)
            {
                Value = -1;
                return;
            }

            foreach (var car in knowledge.Cars)
            {
                if (car.Equals(MyCar) || car.IsOut) continue;

                if (knowledge.GetCurrentPosition(car) == knowledge.GetCurrentPosition(MyCar) - 1
                    && !CarBetween(car)
                    && car.AiData.LastDistanceFromMyCar < 200 && car.AiData.LastDistanceFromMyCar > 0 &&
                    car.PiecePosition.lane.endLaneIndex == MyCar.PiecePosition.lane.endLaneIndex &&
                    car.PiecePosition.lane.startLaneIndex == MyCar.PiecePosition.lane.startLaneIndex &&
                    !car.AiData.IsSlowerThanMe())
                {
                    var outOfCornerAccelerateDistance = knowledge.BendsData.GetBendValues(
                        knowledge.Track.NextOrCurrentTurn(MyCar.PiecePosition.pieceIndex)
                            .GetBendSteepness(MyCar.CurrentLane)).OutOfCornerAccelerateDistance / 2;

                    var myDistanceToBend = knowledge.Track.DistanceToCurrentOrNextBend(MyCar.PiecePosition.pieceIndex,
                                                                                       MyCar.PiecePosition.
                                                                                           inPieceDistance);

                    var distanceToEnemy = car.AiData.LastDistanceFromMyCar;
                    //var slowing = car.AiData.IsSlowingDown();
                    var myV = MyCar.CurrentVelocity;
                    var carV = car.CurrentVelocity;
                    for (int i = 0; i < 60; i++)
                    {
                        if (myDistanceToBend <= 150)
                            break;


                        if (knowledge.BendsData.GetBendValues(
                        knowledge.Track.NextOrCurrentTurn(MyCar.PiecePosition.pieceIndex)
                            .GetBendSteepness(MyCar.CurrentLane)).DesiredVelocity < carV)
                            carV = RaceMath.VelocityOnNextTick(carV);

                        var factor = 0.0;
                        if (MyCar.AvailableTurbo != null)
                            factor = MyCar.AvailableTurbo.Factor;
                        else
                            factor = MyCar.LastTurbo.Factor;

                        myV = RaceMath.VelocityOnNextTick(myV) + RaceMath.Acceleration * factor;
                        myDistanceToBend -= myV * RaceMath.TickDuration;
                        distanceToEnemy = distanceToEnemy - myV * RaceMath.TickDuration + carV * RaceMath.TickDuration;
                        


                        
                        //if (distanceToEnemy > outOfCornerAccelerateDistance / 2 + 50)


                        if (distanceToEnemy < 200 && distanceToEnemy < smallestDistance && car != closest)
                        {
                            Activated = car;
                            closest = car;
                            smallestDistance = distanceToEnemy;
                            Activate();
                            break;
                        }
                    }
                }
            }
            smallestDistance = 999;
        }


        private bool CarBetween(RaceCar targetCar)
        {
            foreach(var car in knowledge.Cars)
            {
                if (car.Equals(MyCar) || car.Equals(targetCar) || car.IsOut)
                    continue;

                if (Track.DistanceToNextCar(MyCar, car) < Track.DistanceToNextCar(MyCar, targetCar) &&
                    Track.DistanceToNextCar(MyCar, car) > 0)
                    return true;
            }
            return false;
        }

        protected void Activate()
        {
            previousDistance = Activated.AiData.LastDistanceFromMyCar;
            Value = 1;

            knowledge.IsBouncing = true;
            myLastSpeed = MyCar.CurrentVelocity;
        }


        public override void Execute(IZTime zTime)
        {
            if (Activated.IsSwitchingLanes() && switchingLaneFor != Track.NextSwitch(MyCar.PiecePosition.pieceIndex).Index && 
                !knowledge.MyCar.IsTurboActive && knowledge.MyCar.IsTurboAvailable)
            {
                if (MyCar.PiecePosition.lane.endLaneIndex < Activated.PiecePosition.lane.endLaneIndex)
                {
                    knowledge.SwitchDirection = SwitchDirection.Right;
                    switchingLaneFor = MyCar.CurrentTrackPiece.GetNext().Index;
                }
                else if (MyCar.PiecePosition.lane.endLaneIndex > Activated.PiecePosition.lane.endLaneIndex)
                {
                    knowledge.SwitchDirection = SwitchDirection.Left;
                    switchingLaneFor = MyCar.CurrentTrackPiece.GetNext().Index;
                }
                else
                {
                    knowledge.SwitchDirection = SwitchDirection.None;
                }
            }
            if (knowledge.MyCar.IsTurboActive || !knowledge.MyCar.IsTurboAvailable || knowledge.LastSentThrottle < 1)
            {
                knowledge.Throttle = 1;
                Log(zTime, "BnTb");
            }
            else
            {
                knowledge.SwitchDirection = SwitchDirection.None;
                knowledge.ActivateTurbo();
                Log(zTime, "BnTb", " BOUNCERTURBO!");
            }
        }
    }
}
