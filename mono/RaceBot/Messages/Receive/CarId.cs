﻿using System;

namespace RaceBot.Messages.Receive
{
	// Helper class used in multiple messages to identify cars.
	public class CarId
	{
		public string name { get; set; }
		public string color { get; set; }
	
		public override string ToString ()
		{
			return String.Format ("{0} ({1})", this.name, this.color);
		}

		public override bool Equals (object obj)
		{
			var id = obj as CarId;
			if (id == null) return false;

			return string.Compare(name, id.name) == 0 && string.Compare(color, id.color) == 0;
		}

		public override int GetHashCode ()
		{
			return base.GetHashCode ();
		}
	}
}

