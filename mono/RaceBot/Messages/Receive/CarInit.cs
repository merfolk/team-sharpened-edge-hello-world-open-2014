﻿using System;

namespace RaceBot.Messages.Receive
{
	/// <summary>
	/// Car details in GameInit message.
	/// </summary>
	public class CarInit
	{
		public CarId id { get; set; }
		public Dimensions dimensions { get; set; }

		public override string ToString ()
		{
			return this.id.ToString ();
		}
	}
		
	public class Dimensions
	{
		public float length { get; set; }
		public float width { get; set; }
		public float guideFlagPosition { get; set; }
	}
}

