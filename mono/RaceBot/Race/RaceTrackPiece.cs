using System;
using System.Collections.Generic;
using System.Linq;
using RaceBot.Messages.Receive;
using RaceBot.Utilities;

namespace RaceBot.Race
{
	/// <summary>
	/// Represents a piece of a race track.
	/// </summary>
	public class RaceTrackPiece
	{
		/// <summary>
		/// Reference to the track that this track piece belongs to.
		/// </summary>
		private RaceTrack raceTrack;

	    /// <summary>
		/// True if the piece can be used for changing lanes, and false otherwise.
		/// </summary>
		public bool HasSwitch { get; private set; }

	    /// <summary>
        /// The (absolute) angle of a bend piece.
	    /// </summary>
	    public double Angle
	    {
            get { return Math.Abs(this.angle); }
	    }

        private double angle;

	    /// <summary>
		/// The length of a straight piece.
		/// </summary>
		private double length;

		/// <summary>
		/// The radius of a bend piece at the center line.
		/// </summary>
        private int radius;

	    /// <summary>
	    /// Constructor.
	    /// </summary>
	    /// <param name="piece">Piece information from gameInit message.</param>
		public RaceTrackPiece(Piece piece)
		{
	        this.HasSwitch = piece.@switch ?? false;
			this.length = piece.length;
			this.radius = piece.radius ?? 0;
			this.angle = piece.angle ?? 0.0f;
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="piece">Piece information from gameInit message.</param>
		/// <param name="raceTrack">Reference to the track that this track piece belongs to.</param>
		public RaceTrackPiece(Piece piece, RaceTrack raceTrack) : this(piece)
		{
			this.raceTrack = raceTrack;
		}

        /// <summary>
        /// Returns the index of this piece in the race track.
        /// </summary>
        public int Index
        {
            get
            {
                return this.GetIndex ();
            }
        }

        /// <summary>
        /// Returns the index of this piece in the race track.
        /// </summary>
        public int GetIndex()
        {
            return this.raceTrack.Pieces.IndexOf (this);
        }

        /// <summary>
        /// Returns the previous track piece.
        /// </summary>
        public RaceTrackPiece GetPrevious()
        {
            return this.raceTrack.PreviousTrackPiece (this.GetIndex ());
        }

        /// <summary>
        /// Returns the next track piece.
        /// </summary>
        public RaceTrackPiece GetNext()
        {
            return this.raceTrack.NextTrackPiece (this.GetIndex ());
        }

        /// <summary>
        /// Returns the next piece which has a switch, and null if not found.
        /// </summary>
        public RaceTrackPiece GetNextSwitch()
        {
            return this.raceTrack.NextSwitch(this.Index);
        }

		/// <summary>
		/// Returns true if the track piece is a straight, and false otherwise.
		/// </summary>
		public bool IsStraight()
		{
			return this.length > 0.0f;
		}

		/// <summary>
		/// Returns true if the track piece is a bend, and false otherwise.
		/// </summary>
		public bool IsBend()
		{
			return (this.radius > 0.0f) && (this.Angle != 0.0f);
		}

        ///// <summary>
        ///// Returns the lane corrected radius for this piece.
        ///// </summary>
        ///// <param name="lane">Current lane.</param>
        //public double GetRadius(RaceTrackLane lane)
        //{
        //    var correctedRadius = this.radius + lane.DistanceFromCenter * Math.Sign(-angle);
        //    return correctedRadius;
        //}

        ///// <summary>
        ///// Returns the lane corrected radius for this piece.
        ///// </summary>
        ///// <param name="lane">Current lane.</param>
        //public double GetRadius(int laneIndex)
        //{
        //    var correctedRadius = this.radius + raceTrack.Lanes[laneIndex].DistanceFromCenter * Math.Sign(-angle);
        //    return correctedRadius;
        //}
	    public double GetSwitchRandomRadius(Lane lane, double positionOnPiece)
	    {
            var radius = this.radius + raceTrack.Lanes[lane.startLaneIndex].DistanceFromCenter * Math.Sign(-angle);
            var eRadius = this.radius + raceTrack.Lanes[lane.endLaneIndex].DistanceFromCenter * Math.Sign(-angle);
	        var percentPos = 1 - positionOnPiece / GetLength(lane);

	        bool switchingDeeper = (lane.startLaneIndex - lane.endLaneIndex) * Math.Sign(-angle) < 0;
            return radius * percentPos + eRadius * (1 - percentPos) - 10 * percentPos * percentPos - 10 * (1 - percentPos) * (1 - percentPos);
	    }


        public double GetRadius(Lane lane)
        {
            var radius = this.radius + raceTrack.Lanes[lane.startLaneIndex].DistanceFromCenter * Math.Sign(-angle);
            if (lane.startLaneIndex == lane.endLaneIndex)
                return radius;

            //radius = this.radius + raceTrack.Lanes[lane.endLaneIndex].DistanceFromCenter * Math.Sign(-angle);
            //var correctedRadius = this.radius + raceTrack.Lanes[lane.startLaneIndex].DistanceFromCenter * Math.Sign(-angle);
            radius = (int)(Math.Min(this.radius + raceTrack.Lanes[lane.endLaneIndex].DistanceFromCenter * Math.Sign(-angle), radius));
            return radius;
        }

        public double GetLength(Lane lane)
        {
            //TODO: if there is a switch?
            if (this.IsStraight())
            {
                if (this.HasSwitch && lane.startLaneIndex != lane.endLaneIndex)
                {
                    var y = this.GetStraightLength();
                    var x = raceTrack.Lanes[lane.startLaneIndex].DistanceFromCenter -
                            raceTrack.Lanes[lane.endLaneIndex].DistanceFromCenter;
                    if (Math.Abs(x) == 20 && Math.Abs(y -100) < 0.001)
                        return 102.060274992934;
                    if (Math.Abs(x) == 20 && Math.Abs(y - 90) < 0.001)
                        return 92.2816812724387;
                    if (Math.Abs(x) == 20 && Math.Abs(y - 78.0) < 0.001)
                        return 80.619013563812 ;

                    return Math.Sqrt(x * x + y * y) * 1.000783334
;
                }
                else
                    return this.GetStraightLength();
            }
            else
            {
                if (this.HasSwitch && lane.startLaneIndex != lane.endLaneIndex)
                {
                    if (radius == 200 && raceTrack.Lanes.Length == 2 && Math.Abs(Math.Abs(angle) - 22.5) < 0.0001)
                    {
                        return 81.05427818;
                    }
                    if (radius == 100 && raceTrack.Lanes.Length == 2 && Math.Abs(Math.Abs(angle) - 45.0) < 0.0001)
                    {
                        return 81.029484142008;
                    }
                    var x1 = 0.0;
                    double y1 = radius + raceTrack.Lanes[lane.startLaneIndex].DistanceFromCenter;

                    var x2 = Math.Sin(angle / 180 * Math.PI) * (radius + raceTrack.Lanes[lane.endLaneIndex].DistanceFromCenter);
                    var y2 = Math.Cos(angle / 180 * Math.PI) * (radius + raceTrack.Lanes[lane.endLaneIndex].DistanceFromCenter);

                    x1 -= x2;
                    y1 -= y2;
                    double d = Math.Sqrt(x1 * x1 + y1 * y1) *1.0073391954420301773703006290289 ;

                    //var y = this.GetBendLength(center);
                    //var x = raceTrack.Lanes[lane.startLaneIndex].DistanceFromCenter -
                    //        raceTrack.Lanes[lane.endLaneIndex].DistanceFromCenter;

                    //var d = Math.Sqrt(x * x + y * y);
                    return d;
                }
                else
                    return GetBendLength(lane);
            }
        }


	    public bool LastTickOnPiece(RaceCar car)
	    {
            var distanceToPieceEnd = GetLength(car.PiecePosition.lane) - car.PiecePosition.inPieceDistance;
            return distanceToPieceEnd < ((RaceMath.VelocityOnNextTick(car.CurrentVelocity) + (car.Acceleration +1)) * RaceMath.TickDuration);
	    }

		/// <summary>
		/// Returns the lenght of a straight piece.
		/// </summary>
        private double GetStraightLength()
		{
			if (this.IsStraight())
			{
				return this.length;
			}

			return 0f;
		}

	    /// <summary>
	    /// Returns the length of a bend piece.
	    /// </summary>
        private double GetBendLength(Lane lane)
		{
			if (this.IsBend())
			{
                var sectorRadius = GetRadius(lane);

                return RaceMath.CircularSectorArcLength (this.Angle, sectorRadius);
			}

			return 0f;
		}

        /// <summary>
        /// Returns the steepness of a bend piece. Bigger value means steeper corner.
        /// </summary>
        public double GetBendSteepness(Lane lane)
        {
            if (!this.IsBend())
                return 0;

            return angle / GetBendLength(lane) * 100;
        }

		/// <summary>
		/// Returns true if the track piece is a left turn, and false otherwise.
		/// </summary>
		public bool IsLeftTurn()
		{
			if (this.IsBend()) {
				// For left turns, angle is negative
                return this.angle < 0.0f;
			}

			return false;
		}

		/// <summary>
		/// Returns true if the track piece is a right turn, and false otherwise.
		/// </summary>
		public bool IsRightTurn()
		{
			if (this.IsBend()) {
				// For right turns, angle is positive
                return this.angle > 0.0f;
			}

			return false;
		}


	   
	}
	
}
