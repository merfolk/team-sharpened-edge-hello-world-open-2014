Source code of Team Sharpened Edge for the Hello World Open 2014 AI competition.

Our C#/Mono solution is in /mono/RaceBot/RaceBot.sln and should work with both Visual Studio and Xamarin Studio.

https://2014.helloworldopen.com/team/516