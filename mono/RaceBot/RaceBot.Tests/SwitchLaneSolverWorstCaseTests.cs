﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using Newtonsoft.Json;
using RaceBot.Messages.Receive;
using RaceBot.Race;
using RaceBot.Utilities;

namespace RaceBot.Tests
{
    [TestFixture]
    public partial class SwitchLaneSolverTests
    {
        private SwitchLaneSolver CreateWorstCase4LaneSolver()
        {
            Track track = TestUtils.CreateWorstCase4LaneSwitchTrack();
            RaceTrack raceTrack = new RaceTrack(track);
            //PrintSwitchPieceIndexes(raceTrack);
            return raceTrack.LaneSolver;
        }

        [Test]
        public void WorstCase4LaneTrackIsCreated()
        {
            // Arrange
            Track track = TestUtils.CreateWorstCase4LaneSwitchTrack();
            RaceTrack raceTrack = new RaceTrack(track);

            // Act

            // Assert
            Assert.AreEqual("WorstCase4Lane", raceTrack.Name);
        }

        [Test]
        public void OptimumLaneWorstCase4LaneSwitch1()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateWorstCase4LaneSolver();

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(0,1));

            // Assert
            Assert.Greater(optimumLaneIndex, 1);
        }

        [Test]
        public void OptimumLaneWorstCase4LaneSwitch2()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateWorstCase4LaneSolver();

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(1,2));

            // Assert
            Assert.Greater(optimumLaneIndex, 2);
        }

        [Test]
        public void OptimumLaneWorstCase4LaneSwitch3()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateWorstCase4LaneSolver();

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(3,3));

            // Assert
            Assert.AreEqual (2, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneWorstCase4LaneSwitch4()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateWorstCase4LaneSolver();

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(4));

            // Assert
            Assert.AreEqual (1, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneWorstCase4LaneSwitch5()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateWorstCase4LaneSolver();

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(5));

            // Assert
            Assert.AreEqual (0, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneWorstCase4LaneSwitch6()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateWorstCase4LaneSolver();

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(13));

            // Assert
            Assert.AreEqual (1, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneWorstCase4LaneSwitch7()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateWorstCase4LaneSolver();

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(21));

            // Assert
            Assert.AreEqual (0, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneWorstCase4LaneSwitch8()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateWorstCase4LaneSolver();

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(31));

            // Assert
            Assert.AreEqual (0, optimumLaneIndex);
        }

        [Test]
        public void OptimumLaneWorstCase4LaneSwitch9()
        {
            // Arrange
            SwitchLaneSolver solver = this.CreateWorstCase4LaneSolver();

            // Act
            int optimumLaneIndex = solver.GetOptimumLane (CreatePiecePosition(41,0));

            // Assert
            Assert.Greater(optimumLaneIndex,0);
        }
    }
}

