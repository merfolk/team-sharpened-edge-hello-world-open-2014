﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Utilities;

namespace RaceBot.Race
{
    public class AiData
    {
        private bool isSlowerThanMe;
        /// <summary>
        /// Returns an enumerable collection of snapshots for this car. Newest is first, oldest is last.
        /// </summary>
        public IEnumerable<RaceCarSnapshot> Snapshots
        {
            get
            {
                return _snapshots.Reverse<RaceCarSnapshot>();
            }
        }

        private Queue<RaceCarSnapshot> _snapshots = new Queue<RaceCarSnapshot>();
        private double slope;
        private const int maximumAmountOfSnapshots = 120;


        /// <summary>
        /// Adds a new snapshot, and removes ones that are too old.
        /// </summary>
        /// <param name="newSnapshot">The new snapshot.</param>
        /// <remarks>>Uses a Queue<T> backwards. Adds newest item to the back, and when the queue
        /// is full starts tossing items out from the front. Iteration is done from back to front.</remarks>
        private void AddSnapshot(RaceCarSnapshot newSnapshot)
        {
            _snapshots.Enqueue(newSnapshot);
            if (_snapshots.Count > maximumAmountOfSnapshots)
            {
                _snapshots.Dequeue(); // Tossing the item on purpose
            }
        }

        public double LastDistanceFromMyCar { get; private set; }

        public double Slope
        {
            get { return slope; }
        }


        public void Clear()
        {
            _snapshots.Clear();
        }

        public void AddSnapshot(RaceCar car, RaceCar myCar, RaceTrack track, long ticks)
        {
            RaceCarSnapshot newSnapshot = new RaceCarSnapshot();
            newSnapshot.GameTick = ticks;
            if (car.IsOut)
            {
                AddSnapshot(newSnapshot);
                return;
            }
            newSnapshot.Velocity = car.CurrentVelocity;
            LastDistanceFromMyCar = track.DistanceToNextCar(myCar, car);
            newSnapshot.DistanceFromMyCar = LastDistanceFromMyCar;
            if (myCar.Dimensions != null &&
                newSnapshot.DistanceFromMyCar < myCar.Dimensions.length + 5 &&
                newSnapshot.DistanceFromMyCar > myCar.Dimensions.length - 5 &&
                car.PiecePosition.lane.startLaneIndex == myCar.PiecePosition.lane.startLaneIndex &&
                car.PiecePosition.lane.endLaneIndex == myCar.PiecePosition.lane.endLaneIndex)
            {
                //highly probable collision
                if (newSnapshot.DistanceFromMyCar < 0)
                    newSnapshot.Collision = MyCarCollision.BackCollision;
                else
                    newSnapshot.Collision = MyCarCollision.FrontCollision;
            }
            
            AddSnapshot(newSnapshot);
            isSlowerThanMe = GetSlowerThanMe();
        }


        public bool IsSlowingDown()
        {
            if (Snapshots.Count() < 10)
                return false;

            if (Snapshots.ElementAt(0).Velocity < Snapshots.ElementAt(1).Velocity &&
                Snapshots.ElementAt(1).Velocity < Snapshots.ElementAt(2).Velocity &&
                Math.Abs(Snapshots.ElementAt(0).Velocity - Snapshots.ElementAt(2).Velocity) < 100)
                return true;
            return false;
        }

        // TODO: IF this method works, and thats a big if, generalize most of
        // this code and just leave the decision at the end. - Matti
        public bool IsSlowerThanMe()
        {
            //return false;
            return isSlowerThanMe;
        }


        private bool GetSlowerThanMe()
        {
            int snapshotNo = Snapshots.Count();

            List<double> ticks = new List<double>(snapshotNo);
            List<double> distancesToMe = new List<double>(snapshotNo);
            int collisionCount = 0;
            var shots = Snapshots;
            for (int i = 0; i < snapshotNo; i++)
            {
                var shot = shots.ElementAt(i);
                if (shot.DistanceFromMyCar < 1000)
                {
                    ticks.Add(shot.GameTick);
                    distancesToMe.Add(shot.DistanceFromMyCar);
                    // must be slow car, we're hitting it from behind
                    if (shot.Collision == MyCarCollision.FrontCollision)
                    {
                        collisionCount++;
                    }
                }
            }
            if (collisionCount > 5)
                return true;

            if (ticks.Count <= 5)
            {
                slope = 0;
                return false;
            }
                

            slope = RaceMath.SlopeOfLinearRegression(ticks.ToArray(), distancesToMe.ToArray());

            if (Slope < 0.0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
