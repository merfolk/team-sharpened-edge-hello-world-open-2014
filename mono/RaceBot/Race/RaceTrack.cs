﻿using System;
using System.Collections.Generic;
using System.Linq;
using RaceBot.Messages.Receive;
using RaceBot.Utilities;

namespace RaceBot.Race
{
	/// <summary>
	/// Represents the race track.
	/// </summary>
	public class RaceTrack
	{
	    private const int maxDistanceToNextCar = 1000;
        private const double MinimumStraightLength = 98d;

	    public string Id { get; private set; }
		public string Name { get; private set; }

		public List<RaceTrackPiece> Pieces { get; private set; }
		public RaceTrackLane[] Lanes { get; private set; }

        public List<Straight> Straights { get; private set; }

        /// <summary>
        /// Returns the longest, main straight on this track, or null if there are no straights.
        /// </summary>
        public Straight MainStraight {
            get
            {
                this.Straights.Sort(); // Should sort from longest to shortest
                return this.Straights.FirstOrDefault();
            }
        }

        /// <summary>
        /// Returns the second longest straight on this track, or null if there are no straights.
        /// </summary>
        /// <value>The second best straight.</value>
        public Straight SecondBestStraight {
            get
            { 
                this.Straights.Sort();
                return this.Straights.Skip(1).FirstOrDefault();
            }
        }

        public SwitchLaneSolver LaneSolver { get; private set; }

		/// <summary>
		/// Contructor.
		/// </summary>
		/// <param name="track">Track data received as a part of gameInit message.</param>
		public RaceTrack (Track track)
		{
            this.Id = track.id;
			this.Name = track.name;

            this.Pieces = InitializePieces (track.pieces);

            InitializeStraights();
            //PrintStraightsToConsole();

            this.Lanes = InitializeLanes (track.lanes);

            this.LaneSolver = new SwitchLaneSolver (this);
		}

        #region Initialization

        private List<RaceTrackPiece> InitializePieces (IEnumerable<Piece> trackPieces)
        {
            List<RaceTrackPiece> raceTrackPieces = new List<RaceTrackPiece> ();

            foreach (Piece piece in trackPieces) {
                var trackPiece = new RaceTrackPiece (piece, this);
                raceTrackPieces.Add (trackPiece);
            }

            return raceTrackPieces;
        }

        private void InitializeStraights()
        {
            this.Straights = new List<Straight>();
            Straight currentStraight = new Straight();

            // Start iterating at first corner, so we don't need to glue straights
            // before and after finish line together
            RaceTrackPiece currentPiece = this.NextOrCurrentTurn(0);
            int startingIndex = currentPiece.Index;

            do
            {
                // Try to add current piece to straight, but
                // if current piece does not fit to the straight...
                if (!currentStraight.Add(currentPiece))
                {
                    if (currentStraight.TotalLength >= MinimumStraightLength)
                    {
                        this.Straights.Add(currentStraight);
                    }

                    currentStraight = new Straight();
                }

                currentPiece = currentPiece.GetNext();
            } while (currentPiece.Index != startingIndex);

            // Add last straight after the loop. Should be part of the loop, though.
            if (currentStraight.TotalLength >= MinimumStraightLength)
            {
                this.Straights.Add(currentStraight);
            }

            Straights.Sort();
        }

        private RaceTrackLane[] InitializeLanes (IEnumerable<LaneInit> trackLanes)
        {
            var raceTrackLanes = new RaceTrackLane[trackLanes.Count()];

            foreach (var lane in trackLanes) {
                raceTrackLanes [lane.index] = new RaceTrackLane (lane.index, lane.distanceFromCenter);
            }

            return raceTrackLanes;
        }

        private void PrintStraightsToConsole()
        {
            Console.WriteLine();
            Console.WriteLine("Straights on this track.");

            foreach (var straight in Straights)
            {
                Console.WriteLine("- Straight " + Straights.IndexOf(straight) + ", total length " + straight.TotalLength + " starts at index " + straight.StartsAt.Index);
            }

            if (MainStraight != null)
            {
                Console.WriteLine("Main straight is: " + MainStraight.StartsAt.Index + " (index), length: " + MainStraight.TotalLength);
            }

            if (SecondBestStraight != null)
            {
                Console.WriteLine("Second best straight is: " + SecondBestStraight.StartsAt.Index + " (index), length: " + SecondBestStraight.TotalLength);
            }
        }

        #endregion

        /// <summary>
        /// Returns the next turn's index.
        /// </summary>
        /// <param name="carOnPiece">Index of the current piece.</param>
        private int NextOrCurrentTurnIndex(int carOnPiece)
	    {
            if (Pieces[carOnPiece].IsBend())
                return carOnPiece;
            else
            {
                var index = carOnPiece;
                while (!Pieces[index].IsBend())
                {
                    index = NextTrackPieceIndex(index);
                }
                return index;
            } 
	    }

        /// <summary>
        /// Returns the next turn piece.
        /// </summary>
        /// <param name="carOnPiece">Index of the current piece.</param>
        public RaceTrackPiece NextOrCurrentTurn(int carOnPiece)
        {
            return Pieces [NextOrCurrentTurnIndex (carOnPiece)];
        }

	    public double DistanceToCurrentOrNextBend(int carOnPiece, double piecePosition)
	    {
	        if (Pieces[carOnPiece].IsBend())
	            return 0;
	        else
	        {
                double distance = -piecePosition;
	            var index = carOnPiece;
	            var lane = new Lane() {startLaneIndex = 0, endLaneIndex = 0};
                while (!Pieces[index].IsBend())
                {
                    distance += Pieces[index].GetLength(lane);
                    index = NextTrackPieceIndex(index);
                }
	            return distance;
	        }
	    }

        /// <summary>
        /// Calculates the distance between two cars.
        /// Only works when cars are on the same lane
        /// </summary>
        /// <param name="car1">First car.</param>
        /// <param name="car2">Second car.</param>
        public double DistanceToNextCar(RaceCar car1, RaceCar car2, bool checkNegativeDistance = true)
        {
            if (car1.PiecePosition == null || car2.PiecePosition == null ||
                car1.PiecePosition.lane == null || car2.PiecePosition.lane == null) return maxDistanceToNextCar;

            var index = car1.PiecePosition.pieceIndex;
            //var index2 = carOnPiece2;
            double distance = -car1.PiecePosition.inPieceDistance;
            distance += car2.PiecePosition.inPieceDistance;

            if (distance < 0 && index == car2.PiecePosition.pieceIndex)
                distance = maxDistanceToNextCar;

            while (index != car2.PiecePosition.pieceIndex)
            {
                distance += Pieces[index].GetLength(car1.PiecePosition.lane);
                index = NextTrackPieceIndex(index);
                if (distance > maxDistanceToNextCar)
                    break;
            }

            

            if (distance > maxDistanceToNextCar)
                distance = maxDistanceToNextCar;

            if (checkNegativeDistance)
            {
                var negativeDistance = DistanceToNextCar(car2, car1, false);
                if (negativeDistance < distance)
                    distance = -negativeDistance;
            }
            
            return distance;
        }

        /// <summary>
        /// Returns the current track piece. Does not check if your index is illegal.
        /// </summary>
        /// <returns>Current piece's index.</returns>
        public RaceTrackPiece CurrentTrackPiece(int index)
        {
            return Pieces[index];
        }

        /// <summary>
        /// Returns index of the next track piece.
        /// </summary>
        /// <param name="index">Current piece's index.</param>
        private int NextTrackPieceIndex(int index)
        {
            index++;
            if (index > Pieces.Count - 1)
                index = 0;
            return index;
        }

        /// <summary>
        /// Returns the next track piece.
        /// </summary>
        /// <param name="index">Current piece's index.</param>
        public RaceTrackPiece NextTrackPiece(int index)
        {
            return Pieces [NextTrackPieceIndex (index)];
        }

        /// <summary>
        /// Returns index of the previous track piece.
        /// </summary>
        /// <param name="index">Current piece's index.</param>
        private int PreviousTrackPieceIndex(int index)
	    {
            index--;
            if (index < 0)
                index = Pieces.Count - 1;
            return index;
	    }

        /// <summary>
        /// Returns the previous track piece.
        /// </summary>
        /// <param name="index">Current piece's index.</param>
        public RaceTrackPiece PreviousTrackPiece(int index)
        {
            return Pieces [PreviousTrackPieceIndex (index)];
        }

        /// <summary>
        /// Returns the next piece which has a switch, and null if not found.
        /// </summary>
        /// <param name="pieceIndex">Current piece index.</param>
        public RaceTrackPiece NextSwitch(int pieceIndex)
        {
            // To prevent an infinite loop on a track which has no switch,
            // only iterate as many times as there are pieces.

            for(int i = 0; i < Pieces.Count; i++)
            {
                pieceIndex = NextTrackPieceIndex (pieceIndex);

                if (Pieces[pieceIndex].HasSwitch) {
                    return Pieces[pieceIndex];
                }
            }

            return null;
        }


	    public double DistanceToPiece(int pieceIndex, PiecePosition position)// int pieceIndex, int carOnPiece, double piecePosition, int laneDistanceFromCenter = 0)
	    {
            if (pieceIndex == position.pieceIndex)
                return 0;
            else
            {
                double distance = -position.inPieceDistance;
                var index = position.pieceIndex;
                while (pieceIndex != index)
                {
                    distance += Pieces[index].GetLength(position.lane);
                    index = NextTrackPieceIndex(index);
                }
                return distance;
            }
	    }
	}
}

