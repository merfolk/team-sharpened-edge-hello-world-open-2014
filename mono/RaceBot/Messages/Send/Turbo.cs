using System;

namespace RaceBot.Messages.Send
{
    public class Turbo : SendMsg
    {
        public string value;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="value">Whatever text you want to send to the server.</param>
        public Turbo(string value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "turbo";
        }
    }
}