﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RaceBot.AI.Learning;
using RaceBot.Messages.Receive;
using RaceBot.Messages.Send;
using RaceBot.Race;
using RaceBot.SEdge.Common;
using RaceBot.Utilities;
using SEdge.AI;
using SEdge.Common;
using RaceTime = RaceBot.Race.RaceTime;

namespace RaceBot.AI
{
    public class RaceKnowledge
    {
        private const int FkTicks = 3;
        private string myCarName;
        private int myCarIndex;
        private bool switchSent = false;
        private int myUpcomingCarLane;
        private int nextSwitchIndex;
        public double LastSentThrottle = 0;
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="enableLearner"></param>
        public RaceKnowledge(bool enableLearner)
        {
            this.RaceTime = new RaceTime();
            EnableLearner = enableLearner;
        }

        public RaceTime RaceTime { get; private set; }

        public SwitchDirection SwitchDirection
        {
            get { return switchDirection; }
            set
            {
                if (switchSent)
                {
                    Logger.WriteLine("Trying to send second switch lane message. Prevented.");
                    return; // cannot change the direction anymore
                }
                    
                switchDirection = value;
                int nextLane;
                if (MyCar.CurrentLane != null)
                    nextLane = MyCar.CurrentLane.endLaneIndex;
                else
                    nextLane = 0;

                if (value == SwitchDirection.Left)
                    nextLane--;
                else if (value == SwitchDirection.Right)
                    nextLane++;
                myUpcomingCarLane = Math.Min(Math.Max(0, nextLane), Track.Lanes.Count()-1);

                var nextSwitch = MyCar.CurrentTrackPiece.GetNextSwitch();
                if (nextSwitch != null)
                {
                    nextSwitchIndex = nextSwitch.Index;
                }
            }
        }

        public RaceBendData BendsData { get; set; }

        public int OtherCarCount
        {
            get { return Cars.Count() - 1; }
        }

        /// <summary>
        /// Returns a reference to our own race car.
        /// </summary>
        public RaceCar MyCar
        {
            get
            {
                return Cars[this.myCarIndex];
            }
        }

        public RaceCar[] Cars { get; set; }

        public RaceTrack Track { get; set; }

        /// <summary>
        /// Current race session.
        /// </summary>
        public RaceSession Session { get; private set; }

        /// <summary>
        /// Returns true if qualifying period is active, and false otherwise.
        /// </summary>
        public bool IsQualifyingPeriod
        {
            get { return Session.durationMs > 0; }
        }

        /// <summary>
        /// Returns the time left for this qualifying period in milliseconds.
        /// </summary>
        /// <value>The qualifying time left.</value>
        public int QualifyingTimeLeft
        {
            get
            {
                if (IsQualifyingPeriod)
                {
                    int timeLeft = this.Session.durationMs - (int)Math.Round(this.RaceTime.TotalD * 1000);
                    return timeLeft;
                }

                return 0;
            }
        }

        public double Throttle { get; set; }

        public RaceLogger Logger { get; set; }

        public bool IsBouncing { get; set; }

        public BendLearner BendLearner { get; private set; }
        public bool EnableLearner 
        {
            get;
            set;
        }

        readonly double[] fkExpectations = new double[FkTicks];
        private int fkIndex = 0;

        public void Initialize(GameInit initData)
        {
            Cars = new RaceCar[initData.race.cars.Count];
            for (int i = 0; i < initData.race.cars.Count; i++)
            {
                var car = initData.race.cars[i];
                Cars[i] = new RaceCar(this, car);
                if (string.CompareOrdinal(myCarName, car.id.name) == 0)
                    myCarIndex = i;
            }

            this.Track = new RaceTrack(initData.race.track);
            this.Session = initData.race.raceSession;

            if (BendLearner == null)
            {
                BendLearner = BendLearner.Create(EnableLearner, Track, Logger);
            }
            switchSent = false;
            SwitchDirection = SwitchDirection.None;
        }

        public void Update(IZTime zTime, CarPosition[] carPositions)
        {
            if (zTime.TotalD < 10)
            {
                CarPosition myCarPosition = carPositions.First(car => car.id.Equals(MyCar.carId));
                CalculateEnvironmentVariables(zTime, myCarPosition);
            }

            foreach (var raceCar in Cars)
            {
                raceCar.Update(zTime, carPositions);
            }
            if (EnableLearner)
                BendLearner.Update(MyCar.Angle, MyCar.PiecePosition);

            if (MyCar.CurrentTrackPiece.HasSwitch && switchSent)
            {
                switchSent = false;
                SwitchDirection = SwitchDirection.None;
            }
        }

        /// <summary>
        /// Calculates possibly changing environment variables such as car engine power,
        /// air resistance, etc.
        /// </summary>
        /// <param name="zTime">Current race time.</param>
        /// <param name="myCarPosition">Position of our own car.</param>
        void CalculateEnvironmentVariables(IZTime zTime, CarPosition myCarPosition)
        {
            //if (MyCar.CurrentLane != null && RaceMath.FkFound)
            //{
            //    var r = RaceMath.GetRadius(myCarPosition.angle - MyCar.Angle, MyCar.Angle, MyCar.TickAngleChange,
            //        MyCar.CurrentVelocity, MyCar.CurrentTrackPiece.GetRadius(MyCar.CurrentLane));
            //    Logger.LogText = string.Format("I: {0} d: {1} l {2} -> {3} r: {4} r2: {5}",
            //        MyCar.PiecePosition.pieceIndex, MyCar.PiecePosition.inPieceDistance,
            //        MyCar.CurrentLane.startLaneIndex, MyCar.CurrentLane.endLaneIndex,
            //        MyCar.CurrentTrackPiece.GetRadius(MyCar.CurrentLane), r);
            //}
            if (zTime.Ticks == 1)
            {
                var velocity = MyCar.CalculateVelocity(zTime, myCarPosition);
                if (Math.Abs(RaceMath.Acceleration - velocity) > 0.00001)
                {
                    RaceMath.Acceleration = velocity;
                    Logger.WriteLine("New Acceleration: " + RaceMath.Acceleration);
                }
                    
            }
            else if (Math.Abs(RaceMath.Acceleration - MyCar.CurrentVelocity) <= 0.00001)
                {
                    var velocity = MyCar.CalculateVelocity(zTime, myCarPosition);
                    var arsx = (MyCar.CurrentVelocity - (velocity - MyCar.Acceleration)) / (MyCar.CurrentVelocity * RaceMath.TickDuration);
                    if (Math.Abs(RaceMath.Arsx - arsx) > 0.00001)
                    {
                        RaceMath.Arsx = arsx;
                        Logger.WriteLine("New air resistance multiplier: " + RaceMath.Arsx);
                    }
                }
            else
            {
                if (!RaceMath.FkFound && myCarPosition.angle != MyCar.Angle && MyCar.CurrentTrackPiece.IsBend() 
                    && !MyCar.IsSwitchingLanes() && Math.Abs(MyCar.VelocityDelta )< MyCar.Acceleration)
                {
                    var Fk = RaceMath.CalculateFkByAngle(myCarPosition.angle - MyCar.Angle, MyCar.Angle, MyCar.TickAngleChange, MyCar.CurrentVelocity,
                        MyCar.CurrentTrackPiece.GetRadius(MyCar.CurrentLane), MyCar.CurrentTrackPiece.IsLeftTurn());

                    if (Fk == 0)
                        return;
                    if (Fk < 200 || Fk > 2500)
                    {
                        Console.WriteLine("Illegal Fk: " + Fk);
                        return;
                    }
                    //RaceMath.Fk = (RaceMath.Fk * fkIndex + Fk) / (fkIndex + 1);
                    //Logger.WriteLine("Fk: " + Fk + " Avg: " + RaceMath.Fk);
                    fkExpectations[fkIndex] = Fk;
                    fkIndex++;

                    if (fkIndex == FkTicks)
                    {
                        var fkAvg = (fkExpectations[0] + fkExpectations[1] + fkExpectations[2]) / FkTicks;
                        if (Math.Abs(fkExpectations[0] - fkExpectations[1]) < 0.00001 &&
                            Math.Abs(fkExpectations[0] - fkExpectations[2]) < 0.00001)
                        {
                            RaceMath.Fk = fkAvg;
                            Logger.WriteLine(string.Format("Fk Found: {0} by using: {1} {2} {3}" , RaceMath.Fk,
                                fkExpectations[0], fkExpectations[1], fkExpectations[2]));
                            RaceMath.FkFound = true;
                        }
                        else
                        {
                            Logger.WriteLine(string.Format("Illegal avg Fk: {0} by using: {1} {2} {3}" , fkAvg,
                                fkExpectations[0], fkExpectations[1], fkExpectations[2]));
                            fkIndex = 0;
                        }
                        //RaceMath.Fk = fkExpectations.Average();
                        
                    }
                }
            }
        }

        public void InitBends(List<BendProperties> bendData)
        {
            this.BendsData = new RaceBendData(bendData);
        }
        /// <summary>
        /// Updates which car is our own.
        /// </summary>
        public void UpdateMyCar(string name)
        {
            this.myCarName = name;
        }

        /// <summary>
        /// Updates lap information.
        /// </summary>
        public void UpdateLap(LapFinished lapFinished)
        {
            RaceCar car = Cars.SingleOrDefault(c => c.carId.Equals(lapFinished.car));
            if (car != null)
            {
                car.UpdateLaps(lapFinished);

                if (car.Equals(MyCar) && TurboDecision.ForcedThrottle)
                {
                    TurboDecision.ResetForcedThrottle();
                    Logger.WriteLine("Forced throttle reset.");
                }
            }
        }

        private long firstCrashTick = 0;
        private long firstSpawnTick = 0;

        /// <summary>
        /// Returns the time it takes for a car to spawn to track after crashing in this race, in ticks.
        /// </summary>
        public long SpawnTimeTicks
        {
            get
            { 
                if (firstCrashTick != 0 && firstSpawnTick != 0)
                {
                    return firstSpawnTick - firstCrashTick;
                }
                else
                {
                    // No one has crashed & spawned yet, so assume
                    // default value for spawn interval
                    return 400;
                }
            }
        }

        /// <summary>
        /// Updates crash information.
        /// </summary>
        public void UpdateCrash(Crash crash)
        {
            if (firstCrashTick == 0 && !IsQualifyingPeriod)
            {
                firstCrashTick = this.RaceTime.Ticks;
            }

            RaceCar car = Cars.SingleOrDefault(c => c.carId.name.Equals(crash.name));
            if (car != null)
            {
                car.UpdateCrash();

                if (car.Equals(MyCar) && NoCarsNear() && !TurboDecision.ForcedThrottle) 
                {
                    Logger.WriteLine("No cars within " + MinCarDistance + " units. Safe to update bend learning.");
                    if (EnableLearner)
                    {
                        BendLearner.UpdateCrash();
                    }
                }

                if (car.Equals(MyCar) && TurboDecision.ForcedThrottle)
                {
                    TurboDecision.ResetForcedThrottle();
                    Logger.WriteLine("Forced throttle reset.");
                }
            }
        }

        private const double MinCarDistance = 150d;

        /// <summary>
        /// Checks that no cars are near currently. Returns true if there ar none, and false otherwise.
        /// </summary>
        private bool NoCarsNear()
        {
            foreach (RaceCar car in Cars)
            {
                if(!car.IsCrashed && !car.Equals(MyCar)) // Note: Crashed cars may have hit us
                {
                    double carDistance = Math.Abs(car.AiData.LastDistanceFromMyCar);

                    if (carDistance < MinCarDistance)
                    {
                        string message = String.Format("{0} is too near, ignoring learn data (distance {1})", car.carId.name, Math.Round(carDistance, 2));
                        Logger.WriteLine(message);
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Updates crash information.
        /// </summary>
        public void UpdateDnf(Dnf crash)
        {
            RaceCar car = Cars.SingleOrDefault(c => c.carId.name.Equals(crash.car.name));
            if (car != null)
            {
                car.UpdateDnf();
            }
        }

        /// <summary>
        /// Updates spawn information.
        /// </summary>
        public void UpdateSpawn(Spawn spawn)
        {
            if (firstSpawnTick == 0 && !IsQualifyingPeriod)
            {
                firstSpawnTick = this.RaceTime.Ticks;
                Logger.WriteLine("New spawn time is " + SpawnTimeTicks + " ticks.");
            }

            RaceCar car = Cars.SingleOrDefault(c => c.carId.name.Equals(spawn.name));
            if (car != null)
            {
                car.UpdateSpawn();
            }
        }

        public void UpdateCarFinished(string name)
        {
            RaceCar car = Cars.SingleOrDefault(c => c.carId.name.Equals(name));
            if (car != null)
            {
                car.UpdateFinished();
            }
        }

        public void RaceFinished()
        {
            if (EnableLearner)
            {
                var filename = Track.Name + ".xml";
                ZSerializer.WriteFileXml(filename, BendLearner, BendLearner.GetType());
            }
        }

        /// <summary>
        /// Returns true if the lane should be switched at the next possible switch location, and false otherwise.
        /// </summary>
        public bool ShouldSwitchLane()
        {
            if (switchSent) return false;
            if (SwitchDirection != SwitchDirection.None &&
                   MyCar.CurrentTrackPiece.GetNextSwitch().Index == MyCar.CurrentTrackPiece.GetNext().Index)
            {
                // last chance to send switch
                if (MyCar.CurrentTrackPiece.LastTickOnPiece(MyCar))
                    return true;

                // second last tick, send if using the same throttle
                var x = (RaceMath.VelocityOnNextTick(RaceMath.VelocityOnNextTick(MyCar.CurrentVelocity) + MyCar.Acceleration *
                                                Throttle) + MyCar.Acceleration *
                                                Throttle)* RaceMath.TickDuration*2;
                if (Throttle >= LastSentThrottle &&
                    MyCar.CurrentTrackPiece.GetLength(MyCar.CurrentLane) < MyCar.PiecePosition.inPieceDistance + x + 0.1)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Let RaceKnowledge know that a switch lane command was sent.
        /// </summary>
        public void SwitchLaneSent()
        {
            Logger.WriteLine("Switch sent: " + SwitchDirection);
            switchSent = true;
        }

        #region Handling of turbo messages

        /// <summary>
        /// Tick of the previous turbo.
        /// </summary>
        private long PreviousTurboOnTick = 0;

        /// <summary>
        /// How many ticks it took the get current turbo and previous turbo.
        /// </summary>
        private long CurrentTurboInterval = 0;

        /// <summary>
        /// How many turbos there have been in the game so far.
        /// </summary>
        public int TotalTurbos { get; private set; }

        private SwitchDirection switchDirection;

        /// <summary>
        /// Game tick when next turbo should be available.
        /// </summary>
        /// <value>The next turbo on tick.</value>
        public long NextTurboOnTick
        {
            get {
                return PreviousTurboOnTick + CurrentTurboInterval;
            }
        }

        /// <summary>
        /// Updates race knowledge with turbo available message.
        /// </summary>
        /// <param name="turbo">The "turboAvailable" message received from server.</param>
        public void UpdateTurboAvailable(TurboAvailable turbo)
        {
            long previousTick = this.RaceTime.Ticks - 1;

            if (TotalTurbos == 0)
            {
                CurrentTurboInterval = previousTick;
                Logger.WriteLine("New turbo interval " + CurrentTurboInterval + " (first turbo).");
            }

            long ticksSinceLastTurbo = previousTick - PreviousTurboOnTick;
            if (ticksSinceLastTurbo != CurrentTurboInterval)
            {
                CurrentTurboInterval = ticksSinceLastTurbo;
                Logger.WriteLine("New turbo interval " + CurrentTurboInterval + ".");
            }

            TotalTurbos++;
            PreviousTurboOnTick = previousTick;

            Logger.WriteLine("Estimated next turbo on tick " + NextTurboOnTick + ".");

            foreach (RaceCar car in Cars)
            {
                car.TurboAvailable(turbo);
            }
        }

        /// <summary>
        /// Updates race knowledge with turbo start message.
        /// </summary>
        /// <param name="name">Name of the car which used a turbo.</param>
        public void UpdateTurboStart(string name)
        {
            foreach (RaceCar car in Cars)
            {
                if (car.carId.name == name)
                {
                    car.TurboStarted();
                    break;
                }
            }
        }

        /// <summary>
        /// Updates race knowledge with turbo end message.
        /// </summary>
        /// <param name="name">Name of the car which used a turbo.</param>
        public void UpdateTurboEnd(string name)
        {
            foreach (RaceCar car in Cars)
            {
                if (car.carId.name == name)
                {
                    car.TurboEnded();
                    break;
                }
            }
        }


        public Lane PredictLane(int pieceIndex)
        {
            if (pieceIndex == MyCar.CurrentTrackPiece.Index || SwitchDirection == SwitchDirection.None)
            {
                return MyCar.CurrentLane;
            }
            return new Lane()
                   {
                       startLaneIndex = PredictEndLane(pieceIndex - 1),
                       endLaneIndex = PredictEndLane(pieceIndex)
                   };
        }

        private int PredictEndLane(int pieceIndex)
        {
            if (pieceIndex > MyCar.PiecePosition.pieceIndex && pieceIndex >= nextSwitchIndex)
                return myUpcomingCarLane;
            return MyCar.CurrentLane.endLaneIndex;
        }

        /// <summary>
        /// Call this when wanting to activate turbo for our own car.
        /// </summary>
        /// <param name="raceTime">Current race time.</param>
        public void ActivateTurbo()
        {
            if (MyCar.IsTurboAvailable && !MyCar.IsTurboActive)
            {
                useTurbo = true;
            }
            else
            {
                if (!MyCar.IsTurboAvailable)
                    Logger.WriteLine("Trying to use turbo, no turbo available!");
                else
                    Logger.WriteLine("Trying to use turbo, turbo already active!");
            }
        }

        /// <summary>
        /// Toggle for when turbo should be activated.
        /// </summary>
        private bool useTurbo = false;

        /// <summary>
        /// Returns true if the bot should use a turbo, and false otherwise.
        /// </summary>
        public bool ShouldUseTurbo()
        {
            if (useTurbo)
            {
                return true;
            }

            return false;
        }


        public void TurboSent()
        {
            Logger.WriteLine("Turbo used: WRUUUUUUUUM!");
            useTurbo = false;
        }


        public void CouldNotSendTurbo()
        {
            Logger.WriteLine("Turbo couldn't be used!");
            useTurbo = false;
        }
        #endregion

        /// <summary>
        /// Returns the car's current position in the race.
        /// </summary>
        /// <returns>The current position.</returns>
        /// <param name="forThisCar">For this car.</param>
        public int GetCurrentPosition(RaceCar forThisCar)
        {
            var orderedCars = Cars.OrderByDescending(c => c.PiecePosition.lap)
                .ThenByDescending(ca => ca.PiecePosition.pieceIndex)
                .ThenByDescending(car => car.PiecePosition.inPieceDistance);

            // Debug print below
//            int i = 1;
//            foreach (var oc in orderedCars)
//            {
//                var message = string.Format("{0}. {1} (L: {2} PI: {3} IPC: {4}",
//                    i, oc.carId.name, oc.PiecePosition.lap, oc.PiecePosition.pieceIndex, oc.PiecePosition.inPieceDistance);
//                Console.WriteLine(message);
//                i++;
//            }

            return orderedCars.ToList().IndexOf(forThisCar) + 1;
        }
    }
}
