﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SEdge.Common;

namespace RaceBot.AI.Decisions.Test
{
    public class BrakeTestState:BaseDecision
    {
        private bool activated = true;
        public BrakeTestState(RaceKnowledge knowledge) : base(knowledge)
        {
        }


        public override bool PassThrough
        {
            get { return false; }
        }


        public override void CalculateDecisionValue()
        {
            if (activated || MyCar.CurrentVelocity > 550)
            {
                if (MyCar.CurrentVelocity <= 1 && MyCar.CurrentVelocity >= 0)
                {
                    Value = -1;
                    activated =false;
                }
                else
                {
                    Value = 1;
                    activated = true;
                }
            }
            else
            {
                Value = -1;
            }
        }


        public override void Execute(IZTime zTime)
        {
            knowledge.Throttle = 0;
        }
    }
}
