﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Messages.Send;
using RaceBot.Race;
using SEdge.Common;

namespace RaceBot.AI.Decisions
{
    public class HardCodedSwitchLaneDecision : BaseDecision
    {
        private int lastIndex = 999;
        public List<LaneSwitch> LaneSwitches;

        public override bool PassThrough
        {
            get { return true; }
        }

        public HardCodedSwitchLaneDecision(RaceKnowledge knowledge)
            : base(knowledge)
        {
            if (string.Compare (Track.Name, "keimola", ignoreCase: true) == 0) {
                LaneSwitches = CreateFinlandSwitches ();
            } else if (String.Compare (Track.Name, "germany", ignoreCase: true) == 0) {
                LaneSwitches = CreateGermanySwitches ();
            } else {
                LaneSwitches = new List<LaneSwitch> ();
            }
        }


        private static List<LaneSwitch> CreateFinlandSwitches()
        {
            var list = new List<LaneSwitch>();
            list.Add(new LaneSwitch(3, SwitchDirection.Right));
            list.Add(new LaneSwitch(5, SwitchDirection.Left));
            list.Add(new LaneSwitch(14, SwitchDirection.Right));
            return list;
        }

        private static List<LaneSwitch> CreateGermanySwitches()
        {
            // 1, 14, 21, 27, 36, 45, 53
            var list = new List<LaneSwitch>();
            list.Add(new LaneSwitch(0, SwitchDirection.Left));
            list.Add(new LaneSwitch(13, SwitchDirection.Right));
            list.Add(new LaneSwitch(27, SwitchDirection.Left));
            list.Add(new LaneSwitch(35, SwitchDirection.Right));
            list.Add(new LaneSwitch(52, SwitchDirection.Left));
            return list;
        }


        public override void CalculateDecisionValue()
        {
            Value = -1;

            var nextIndex = MyCar.CurrentTrackPiece.GetNext().Index;

            if (nextIndex != lastIndex)
            {
                foreach (var laneSwitch in LaneSwitches)
                {
                    if (laneSwitch.PieceIndex == nextIndex)
                    {
                        Value = 1;
                    }
                }
            }
            lastIndex = nextIndex;
        }


        public override void Execute(IZTime zTime)
        {
            var nextIndex = MyCar.CurrentTrackPiece.GetNext().Index;

            foreach (var laneSwitch in LaneSwitches)
            {
                if (laneSwitch.PieceIndex == nextIndex)
                {
                    knowledge.SwitchDirection = laneSwitch.Direction;
                }
            }

        }
    }
}
