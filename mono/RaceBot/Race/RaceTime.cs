﻿using System;
using SEdge.Common;

namespace RaceBot.Race
{
    public class RaceTime : IZTime
    {
        public TimeSpan ElapsedTime { get; set; }
        public TimeSpan TotalTime { get; set; }

        float f;
        double d;
        float totalF;
        double totalD;

        public long Ticks { get; private set; }

        /// <summary> Gets elapsed game time as float</summary>
        public float F
        { get { return f; } }

        /// <summary> Gets elapsed game time as double</summary>
        public double D
        { get { return d; } }

        /// <summary> Gets total game time as float</summary>
        public float TotalF
        { get { return totalF; } }
        /// <summary> Gets total game time as double</summary>
        public double TotalD
        { get { return totalD; } }

        /// <summary>
        /// Constructor.
        /// </summary>
        public RaceTime()
        {
            this.Reset();
        }

        /// <summary>
        /// Resets the race time clock.
        /// </summary>
        private void Reset()
        {
            Ticks = 0;
            f = 0;
            d = 0;
            totalF = 0;
            totalD = 0;
            ElapsedTime = new TimeSpan();
            TotalTime = new TimeSpan();
        }

        /// <summary>
        /// Increments gametime by one tick.
        /// </summary>
        private void FurtherTick()
        {
            Ticks++;
            f = 1f / 60f;
            d = 1d / 60d;
            ElapsedTime = new TimeSpan(166667);

            totalF += f;
            totalD += d;
            TotalTime += ElapsedTime;
        }

        /// <summary>
        /// Updates game time.
        /// </summary>
        /// <param name="gameTick">Latest game tick received from server.</param>
        public void Update(long newGameTick)
        {
            if (newGameTick == 0)
            {
                this.Reset();
            }

            // Duplicate tick received (e.g. lap finished, car crashed...)
            if (Ticks == newGameTick)
            {
                return;
            }

            if (Ticks + 1 != newGameTick)
            {
                // TO DO: Recover fro missing a game tick, just calling FurtherTick() will not get us into sync!
                // We need to pass the real gameTick.
                Console.WriteLine ("A TICK WAS MISSED!!! last tick: " + Ticks + " new tick: " + newGameTick);
            }

            this.FurtherTick ();
        }
    }
}
