﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Messages.Receive;
using RaceBot.Utilities;
using SEdge.Common;

namespace RaceBot.AI.Decisions
{
    public class SpinPreventerPrime : BaseDecision
    {
        protected double lastAngle;
        protected double maxAngle;
        protected int predictTicks;
        protected double endOfTurnDistance;
        public bool Straigth;
        protected int crashIndex = -1;
        protected virtual double Throttle { get; set; }
        protected int ticksTillMax = 0;
        protected int ticksTillCrash = 0;
        protected SpinMother spinMother;

        public SpinPreventerPrime(RaceKnowledge knowledge, int predictTicks) : base(knowledge)
        {
            this.predictTicks = predictTicks;
        }


        protected bool WillCrashOnSpeed(bool exitWhenStraigth = true, double i1 = -1)
        {
            var crashAngle = i1;
            if (crashAngle < 0)
                crashAngle = RaceMath.CrashAngle;

            var angle = MyCar.Angle;
            var tickChange = MyCar.TickAngleChange;
            var v = MyCar.CurrentVelocity;
            var d = MyCar.PiecePosition.inPieceDistance;
            var piece = MyCar.CurrentTrackPiece;

            
            maxAngle = angle;
            bool left = piece.IsLeftTurn();
            double adj;
            for (int i = 0; i < predictTicks; i++)
            {
                var predictLane = knowledge.PredictLane(piece.Index);
                var pieceRadius = piece.GetRadius(predictLane);
                if (piece.IsBend())
                {
                    adj = knowledge.BendLearner.GetBendAdjustment(piece.Index);
                    //if (predictLane.endLaneIndex != predictLane.startLaneIndex)
                    //{
                    //    crashAngle = 59;
                    //    //adj -= 20;
                    //    //pieceRadius = piece.GetSwitchRandomRadius(predictLane, d);
                    //}
                }
                else
                {
                    adj = 0;
                }


                
                var a = RaceMath.AngleAccelOnNextTick(angle, tickChange, v, pieceRadius,
                    piece.IsLeftTurn(), adj);




                v = RaceMath.VelocityOnNextTick(v) + MyCar.PredictAccel(i, knowledge.RaceTime.TotalD) * Throttle;
                d += v * RaceMath.TickDuration;
                if (d > piece.GetLength(predictLane))
                {
                    d -= piece.GetLength(predictLane);
                    piece = piece.GetNext();

                    if (predictLane.endLaneIndex != predictLane.startLaneIndex && crashAngle != 60)
                    {
                        var vBonus = Math.Max((v - 300)  / 50, 3.0);
                        crashAngle = Math.Min(crashAngle, 60 - vBonus);
                    }
                        
                }
                
                endOfTurnDistance -= v * RaceMath.TickDuration;

                

                tickChange += a;
                angle += tickChange;

                
                if (Math.Abs(maxAngle) < Math.Abs(angle))
                {
                    maxAngle = angle;
                    ticksTillMax = i;
                }
                lastAngle = angle;
                ticksTillCrash = i;
                crashIndex = piece.Index;

                if (angle < -crashAngle || angle > crashAngle)
                    return true;
                if (exitWhenStraigth)
                {
                    if (left && angle > -crashAngle && tickChange >= 0
                        && !piece.IsLeftTurn())
                        return false; // left turns won't kill us

                    if (!left && angle < crashAngle && tickChange <= 0
                        && !piece.IsRightTurn())
                        return false; // right turns won't kill us
                }
                
            }

            return false;
        }


        


        public override bool PassThrough
        {
            get { return false; }
        }

        public override void CalculateDecisionValue()
        {
            if (knowledge.IsBouncing)
            {
                Value = -1;
                return;
            }
                
            var currentPiece = MyCar.CurrentTrackPiece;
            var nextPiece = currentPiece.GetNext();

            var predictLane = knowledge.PredictLane(nextPiece.Index);

            endOfTurnDistance = nextPiece.GetLength(predictLane) - MyCar.PiecePosition.inPieceDistance;

            //var speedDelta = (lastAngleSpeed - MyCar.AngleSpeed) * RaceMath.TickDuration;
            Straigth = false;
            Throttle = knowledge.Throttle;
            if (WillCrashOnSpeed(exitWhenStraigth: Straigth))
                Value = 1; // only activate in corners
            else
                Value = -1;
        }


        public override void Execute(IZTime zTime)
        {
            //if ((maxAngle > 0 && MyCar.CurrentTrackPiece.IsLeftTurn()) || (maxAngle < 0 && MyCar.CurrentTrackPiece.IsRightTurn()))
            //    knowledge.Throttle = 1;
            //else
                knowledge.Throttle = 0;
            //knowledge.Throttle = Math.Max(Math.Min((endOfTurnDistance + 10) / 200, 1), 0);
            Log(zTime, "SpPP", " " + ZLib.FloatToText(maxAngle));
        }
    }
}
