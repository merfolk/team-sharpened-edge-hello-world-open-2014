﻿using System;
using RaceBot.AI;
using RaceBot.AI.Decisions;
using RaceBot.Messages.Receive;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using RaceBot.Messages.Send;
using RaceBot.Race;
using RaceBot.Utilities;

namespace RaceBot.Tests
{
    public class TestUtils
    {
        public const string PathToKeimolaTrack = "../../TestData/keimola.txt";
        public const string PathToGermanyTrack = "../../TestData/germany.txt";
        public const string PathToUsaTrack = "../../TestData/usa.txt";
        public const string PathToFranceTrack = "../../TestData/france.txt";

        /// <summary>
        /// Reads a full "gameInit" message from a file, and returns the track contained in it.
        /// </summary>
        public static Track ReadTestTrackFromFile(string pathToFile)
        {
            string allText = File.ReadAllText (pathToFile);

            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper> (allText);
            var gameInitData = JsonConvert.DeserializeObject<GameInit> (msg.data.ToString ());

            return gameInitData.race.track;
        }

        public static RaceTrack LoadKeimola()
        {
            Track track = TestUtils.ReadTestTrackFromFile (TestUtils.PathToKeimolaTrack);
            return new RaceTrack (track);
        }

        public static RaceTrack LoadGermany()
        {
            Track track = TestUtils.ReadTestTrackFromFile (TestUtils.PathToGermanyTrack);
            return new RaceTrack (track);
        }

        public static RaceTrack LoadUsa()
        {
            Track track = TestUtils.ReadTestTrackFromFile (TestUtils.PathToUsaTrack);
            return new RaceTrack (track);
        }

        public static RaceTrack LoadFrance()
        {
            Track track = TestUtils.ReadTestTrackFromFile (TestUtils.PathToFranceTrack);
            return new RaceTrack (track);
        }

        public static RaceKnowledge CreateRaceKnowledge(Track track, List<PiecePosition> positions)
        {
            var myCar = CreateMyCar();
            var car = CreateOtherCar();

            var knowledge = new RaceKnowledge(false);
            var gameinit = new GameInit();
            gameinit.race = new Messages.Receive.Race();
            gameinit.race.track = track;

            var carPosition = CreateCarPositions(positions);
            gameinit.race.cars = new List<CarInit> { myCar, car };
            gameinit.race.raceSession = new RaceSession { laps = 3, maxLapTimeMs = 6000, durationMs = 0, quickRace = true };
            var raceTime = new Race.RaceTime();
            knowledge.Logger = new RaceLogger(knowledge, false, false, "testbot", false);
            knowledge.Initialize(gameinit);
            knowledge.UpdateMyCar(myCar.id.name);
            knowledge.Update(raceTime, carPosition);
            return knowledge;
        }

        private static CarInit CreateMyCar()
        {
            var myCar = new CarInit();
            myCar.id = new CarId() { color = "red", name = "Sharpened Edge" };
            return myCar;
        }

        private static CarInit CreateOtherCar()
        {
            var car = new CarInit();
            car.id = new CarId() { color = "blue", name = "Mopo" };
            return car;
        }

        public static CarPosition[] CreateCarPositions(List<PiecePosition> positions)
        {
            var myCar = CreateMyCar();
            var car = CreateOtherCar();

            var cp1 = new CarPosition()
            {
                angle = 0,
                id = myCar.id,
                piecePosition = positions[0]
            };

            var cp2 = new CarPosition()
            {
                angle = 0,
                id = car.id,
                piecePosition = positions[1]
            };

            return new CarPosition[] {cp1, cp2};
        }

        public static Track CreateWorstCase4LaneSwitchTrack()
        {
            Track track = new Track();
            track.name = "WorstCase4Lane";

            track.pieces = new List<Piece>();
            // direction right
            track.pieces.Add(new Piece { length = 100.0f}); // Straight
            track.pieces.Add(new Piece { length = 100.0f, @switch = true }); // Straight
            track.pieces.Add(new Piece { length = 100.0f, @switch = true }); // Straight

            
            track.pieces.Add(new Piece { radius = 100, angle = 22.5f }); // Right turn
            track.pieces.Add(new Piece { radius = 100, angle = 22.5f, @switch = true }); // Right turn
            track.pieces.Add(new Piece { radius = 100, angle = 22.5f, @switch = true }); // Right turn
            track.pieces.Add(new Piece { radius = 100, angle = 22.5f, @switch = true }); // Right turn


            // direction down
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f, @switch = true }); // left turn

            // direction up
            track.pieces.Add(new Piece { radius = 100, angle = 22.5f }); // Right turn
            track.pieces.Add(new Piece { radius = 100, angle = 22.5f }); // Right turn
            track.pieces.Add(new Piece { radius = 100, angle = 22.5f }); // Right turn
            track.pieces.Add(new Piece { radius = 100, angle = 22.5f}); // Right turn

            // direction right 
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f, @switch = true }); // left turn

            // direction up
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn

            // direction left
            track.pieces.Add(new Piece { length = 100.0f }); // Straight
            track.pieces.Add(new Piece { length = 100.0f }); // Straight
            track.pieces.Add(new Piece { length = 100.0f }); // Straight
            track.pieces.Add(new Piece { length = 100.0f }); // Straight
            track.pieces.Add(new Piece { length = 100.0f }); // Straight

            track.pieces.Add(new Piece { length = 100.0f, @switch = true }); // Straight
            track.pieces.Add(new Piece { length = 100.0f }); // Straight


            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn

            track.pieces.Add(new Piece { length = 50.0f }); // Straight

            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f }); // left turn
            track.pieces.Add(new Piece { radius = 100, angle = -22.5f, @switch = true }); // left turn

            track.lanes = new List<LaneInit>();
            track.lanes.Add(new LaneInit { index = 0, distanceFromCenter = -30 });
            track.lanes.Add(new LaneInit { index = 1, distanceFromCenter = -10 });
            track.lanes.Add(new LaneInit { index = 2, distanceFromCenter = 10 }); 
            track.lanes.Add(new LaneInit { index = 3, distanceFromCenter = 30 }); 

            return track;
        }

        public static Track CreateCircleTrackWithNoSwitches()
        {
            Track track = new Track();
            track.name = "Circle";

            track.pieces = new List<Piece>();

            track.pieces.Add(new Piece { radius = 200, angle = 90 });
            track.pieces.Add(new Piece { radius = 200, angle = 90 });
            track.pieces.Add(new Piece { radius = 200, angle = 90 });
            track.pieces.Add(new Piece { radius = 200, angle = 90 });

            track.lanes = new List<LaneInit>();
            // Lane indeces are in incorrect order on purpose, just making sure that it won't completely crash our bot
            track.lanes.Add(new LaneInit { index = 1, distanceFromCenter = -10 });
            track.lanes.Add(new LaneInit { index = 0, distanceFromCenter = 10 });

            return track;
        }
    }
}

