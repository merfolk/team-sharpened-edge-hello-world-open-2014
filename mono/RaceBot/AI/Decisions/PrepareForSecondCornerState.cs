﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Utilities;
using SEdge.Common;
using RaceBot.Race;

namespace RaceBot.AI.Decisions
{
    [Obsolete("Will get to an infinite loop on a track with no straight pieces. You have been warned!")]
    public class PrepareForSecondCornerState : CorneringState
    {
        public PrepareForSecondCornerState(RaceKnowledge knowledge) : base(knowledge)
        {
            logName = "Crn2";
        }


        public override bool PassThrough
        {
            get { return false; }
        }

        public override void CalculateDecisionValue()
        {
            var myCarOnPiece = MyCar.PiecePosition.pieceIndex;

            var nextBend = Track.NextOrCurrentTurn (myCarOnPiece);
            RaceTrackPiece secondBend = nextBend;
            var predictLane = knowledge.PredictLane(secondBend.Index);
            var previousSteepness = nextBend.GetBendSteepness(predictLane);
            while (Math.Abs(previousSteepness -
                secondBend.GetBendSteepness(predictLane)) < 0.0001 ||
                secondBend.IsStraight())
            {
                secondBend = secondBend.GetNext ();
                if (secondBend.IsStraight())
                    previousSteepness = 0;
            }

            predictLane = knowledge.PredictLane(secondBend.Index);
            var steepness = secondBend.GetBendSteepness(predictLane);
            //if (Math.Abs(steepness) >
            //    Math.Abs(secondBend.GetBendSteepness(distanceFromCenter)))
            //{
            //    Value = -1;
            //    return;
            //}

            double distance = CalculateBrakeDistance(steepness, secondBend.GetIndex(), myCarOnPiece);


            var brakingDistance = RaceMath.BreakingDistance(
                RaceMath.GetTickFromVelocity(MyCar.CurrentVelocity),
                RaceMath.GetTickFromVelocity(desiredVelocity)) - lateBrakeDistance;

            if (distance < brakingDistance)
            {
                //Console.WriteLine("Prepare second corner steepness: " + ZLib.FloatToText(steepness));
                Value = 1; // activate
            }
            else
                Value = -1; // deactivate

        }
    }
}
