﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RaceBot.Messages.Receive
{
	/// <summary>
	/// "The gameInit message describes the racing track, the current racing session and the cars on track."
	/// </summary>
	public class GameInit
	{
		public Race race { get; set; }

		public override string ToString ()
		{
			StringBuilder builder = new StringBuilder ();

			builder.AppendLine ();
            builder.AppendLine ("GAME INIT");
			builder.AppendLine ();
            builder.AppendFormat ("Track: {0}", this.race.track.name);
			builder.AppendLine ();

            builder.AppendFormat("Lanes: {0} total, ", this.race.track.lanes.Count);
            foreach (var lane in this.race.track.lanes)
            {
                builder.AppendFormat("[Index: {0} Offset: {1}] ", lane.index, lane.distanceFromCenter);
            }
            builder.AppendLine ();

            var raceSession = this.race.raceSession;

            builder.AppendFormat("Laps: {0} (race), Duration: {1} ms (qualifying), Max lap time: {2} ms, Quick race: {3}",
                raceSession.laps, raceSession.durationMs, raceSession.maxLapTimeMs, raceSession.quickRace);
			builder.AppendLine ();
			builder.AppendLine ("Cars:");

            int i = 1;
			foreach (CarInit car in this.race.cars) {
                builder.AppendLine (i + ". " + car.ToString ());
                AlertIfCarValuesAreNotDefault(builder, car);
                i++;
			}

			builder.AppendLine ();

			return builder.ToString ();
		}

        static void AlertIfCarValuesAreNotDefault(StringBuilder builder, CarInit car)
        {
            if (car.dimensions.guideFlagPosition != 10.0d)
            {
                string message = String.Format("!!! Car '{0}' has guide flag position {1} (expected default 10.0) !!!", car.id.name, car.dimensions.guideFlagPosition);
                builder.AppendLine(message);
            }

            if (car.dimensions.length != 40.0d)
            {
                string message = String.Format("!!! Car '{0}' has length {1} (expected default 40.0) !!!", car.id.name, car.dimensions.length);
                builder.AppendLine(message);
            }

            if (car.dimensions.width != 20.0d)
            {
                string message = String.Format("!!! Car '{0}' has width {1} (expected default 20.0) !!!", car.id.name, car.dimensions.width);
                builder.AppendLine(message);
            }
        }
	}

	public class Race
	{
		public Track track { get; set; }
		public List<CarInit> cars { get; set; }
		public RaceSession raceSession { get; set; }
	}

	public class Track
	{
		public string id { get; set; }
		public string name { get; set; }
		public List<Piece> pieces { get; set; }
		public List<LaneInit> lanes { get; set; }
	}

	public class Piece
	{
		public double length { get; set; }
		public bool? @switch { get; set; }
		public int? radius { get; set; }
		public double? angle { get; set; }
	}

	/// <summary>
	/// Lane format in "GameInit". Different from Lane in "CarPositions".
	/// </summary>
	public class LaneInit
	{
		public int distanceFromCenter { get; set; }
		public int index { get; set; }
	}
        
    /// <summary>
    /// Qualifying: "raceSession": { "durationMs": 20000 }
    /// Race: "raceSession": { "laps": 5, "maxLapTimeMs": 60000, "quickRace": false }
    /// </summary>
	public class RaceSession
	{
		public int laps { get; set; }

        /// <summary>
        /// Indicates qualifying state length.
        /// </summary>
        public int durationMs { get; set; }

		public int maxLapTimeMs { get; set; }
		public bool quickRace { get; set; }
	}
}

