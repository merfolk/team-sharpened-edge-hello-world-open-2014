﻿using System;
using RaceBot.Race;
using System.Collections.Generic;
using System.Linq;
using RaceBot.Messages.Receive;

namespace RaceBot
{
    /// <summary>
    /// A higher level class from RaceTrackPiece, which represents longer straights.
    /// </summary>
    public class Straight : IComparable<Straight>
    {
        // IComparable<T> implementation
        public int CompareTo(Straight straight)
        {
            return this.TotalLength.CompareTo(straight.TotalLength) * -1; // Times -1 for reversing the compare order. Longer should come before last.
        }

        // Individual track pieces that belong to this straight.
        private List<RaceTrackPiece> trackPieces;

        /// <summary>
        /// First track piece of this straight.
        /// </summary>
        public RaceTrackPiece StartsAt
        {
            get
            { 
                return this.trackPieces.First(); // Making an assumption that the pieces are in the correct order...
            }
        }

        public double TotalLength
        {
            get 
            {
                // Don't care about lanes when on a straight, they should
                // make no difference.
                Lane lane = new Lane { startLaneIndex = 0, endLaneIndex = 0 };
                return this.trackPieces.Sum(tp => tp.GetLength(lane));
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="trackPieces">Track pieces.</param>
        public Straight()
        {
            this.trackPieces = new List<RaceTrackPiece>();
        }

        /// <summary>
        /// Adds a track piece to this straight. Returns true if the track piece was added, and false otherwise.
        /// </summary>
        /// <param name="trackPiece">Track piece to add.</param>
        public bool Add(RaceTrackPiece trackPiece)
        {
            if (trackPiece.IsStraight())
            {
                this.trackPieces.Add(trackPiece);
                return true;
            }

            return false;
        }
    }
}

