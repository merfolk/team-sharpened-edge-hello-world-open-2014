﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Messages.Send;
using RaceBot.Race;
using SEdge.Common;

namespace RaceBot.AI.Decisions
{
    public class OnlyOneLane : BaseDecision
    {
        private int laneNumber;
        public List<LaneSwitch> LaneSwitches;

        public override bool PassThrough
        {
            get { return true; }
        }

        public OnlyOneLane(RaceKnowledge knowledge, int laneNumber)
            : base(knowledge)
        {
            LaneSwitches = new List<LaneSwitch>();
            this.laneNumber = laneNumber;
        }

        public override void CalculateDecisionValue()
        {
            Value = -1;

            if (MyCar.CurrentTrackPiece.GetNext().HasSwitch && MyCar.PiecePosition.lane.startLaneIndex != laneNumber)
            {
                Value = 1;
            }
        }

        public override void Execute(IZTime zTime)
        {
            if (zTime.Ticks < 3) return;
            if (MyCar.PiecePosition.lane.startLaneIndex > laneNumber
                && MyCar.PiecePosition.lane.endLaneIndex > 0)
            {
                knowledge.SwitchDirection = SwitchDirection.Left;
            }
            else if (MyCar.PiecePosition.lane.endLaneIndex < Track.Lanes.Length - 1)
            {
                knowledge.SwitchDirection = SwitchDirection.Right;
            }
        }
    }
}
