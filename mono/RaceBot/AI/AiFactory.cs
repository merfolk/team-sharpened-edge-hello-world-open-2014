﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.AI.Decisions;
using RaceBot.AI.Decisions.Test;
using RaceBot.AI.Learning;
using RaceBot.Race;
using RaceBot.Utilities;
using SEdge.AI;

namespace RaceBot.AI
{
    public static class AiFactory
    {
        public enum AiType
        {
            Prime,
            PrimeOld,
            PrimeNotLearning,
            PrimeKiller,
            Const,
            Mopo,
            Bender,
            TheOne,
            Blocker,
            BrakeTest,
            CornerTest,
            SpinRacer
        }

        public static RacingAI GenerateAi(RaceKnowledge knowledge, AiType type, double additionalValue = 0)
        {
            knowledge.Logger.WriteLine("Created: " + type.ToString());
            switch (type)
            {
                case AiType.Prime:
                case AiType.PrimeNotLearning:
                    return GeneratePrimeAi(knowledge, type == AiType.Prime);
                case AiType.PrimeOld:
                    return GenerateOldPrimeAi(knowledge);
                case AiType.PrimeKiller:
                    return GeneratePrimeKiller(knowledge);
                case AiType.Const:
                    return GenerateConstThrottleAi(knowledge, additionalValue);
                case AiType.Mopo:
                    return GenerateMopoAi(knowledge);
                case AiType.Bender:
                    return GenerateBender(knowledge);
                case AiType.TheOne:
                    return GenerateTheOne(knowledge, (int)additionalValue);
                case AiType.Blocker:
                    return GenerateBlocker(knowledge);
                case AiType.BrakeTest:
                    return GenerateBrakeTestAi(knowledge);
                case AiType.CornerTest:
                    return GenerateCornerTestAi(knowledge, (int)additionalValue);
                case AiType.SpinRacer:
                    return GenerateSpinRacerAi(knowledge);
                default:
                    return GenerateConstThrottleAi(knowledge, 4);
            }
        }


        public static RacingAI GenerateSpinRacerAi(RaceKnowledge knowledge)
        {
            DecisionMaker dMaker = new DecisionMaker() { DecisionStyle = DecisionStyle.FirstActive };

            //dMaker.Add(GenerateFrictionLearner(knowledge));

            dMaker.Add(new TurboDecision(knowledge));
            dMaker.Add(new TurboBouncerState(knowledge));
            dMaker.Add(new BouncerState(knowledge));
            //dMaker.Add(new OnlyOneLane(knowledge, 0));
            //dMaker.Add(new ConstThrottleState(knowledge, 0.7));
            SpinMother spinMother = new SpinMother();





            //dMaker.Add(new SwitchLaneDecision(knowledge));
            dMaker.Add(new SwitchLaneDodge(knowledge));
            //dMaker.Add(new SpinPreventerPrime(knowledge, 50) { Straigth = true });    
            //dMaker.Add(new ConstThrottleState(knowledge, 0.5));
            knowledge.InitBends(BendDataLibrary.AddBendsSpinRacer());
            var bendForce = 750;
            var predict = 75;
            knowledge.EnableLearner = true;
            
            for (int i = 73; i <= predict; i++)
            {
                dMaker.Add(new SpeedStopperPrime(knowledge, predict, predict - i, spinMother));
            }

            int ticks = 75;

            int runnerCount= 20;
            dMaker.Add(new SpeedRunnerPrime(knowledge, ticks, 1.0, true));
            for (int i = runnerCount-1; i >= 0; i--)
            {
                dMaker.Add(new SpeedRunnerPrime(knowledge, ticks, 1.0/runnerCount * i, true));
            }
            //dMaker.Add(new SpeedRunnerPrime(knowledge, ticks, 1.0, true));
            //dMaker.Add(new SpeedRunnerPrime(knowledge, ticks, 0.875, true));
            ////dMaker.Add(new AntiSpinState(knowledge, 15, 1, 0.8));
            //dMaker.Add(new SpeedRunnerPrime(knowledge, ticks, 0.75, true));
            //dMaker.Add(new SpeedRunnerPrime(knowledge, ticks, 0.625, true));
            ////dMaker.Add(new AntiSpinState(knowledge, 15, 1, 0.6));
            //dMaker.Add(new SpeedRunnerPrime(knowledge, ticks, 0.5, true));
            ////dMaker.Add(new AntiSpinState(knowledge, 15, 1, 0.4));
            //dMaker.Add(new SpeedRunnerPrime(knowledge, ticks, 0.375, true));
            //dMaker.Add(new SpeedRunnerPrime(knowledge, ticks, 0.25, true));
            ////dMaker.Add(new AntiSpinState(knowledge, 15, 1, 0.2));
            //dMaker.Add(new SpeedRunnerPrime(knowledge, ticks, 0.125, true));
            //dMaker.Add(new SpeedRunnerPrime(knowledge, ticks, 0, false));
            //dMaker.Add(new SpeedRunnerPrime(knowledge, 25, 0.125));
            //dMaker.Add(new UpcomingCornerStatePrime(knowledge, bendForce));
            //dMaker.Add(new OutOfCornerPrime(knowledge));
            //dMaker.Add(new CorneringStatePrime(knowledge, bendForce));              // 2nd
            //dMaker.Add(new FullOnStraightState(knowledge));
            //dMaker.Add(new AntiSpinState(knowledge, 20, 20, spinMother));
            //dMaker.Add(new AntiSpinState(knowledge, 20, 15, spinMother));
            //dMaker.Add(new AntiSpinState(knowledge, 20, 10, spinMother));
            //dMaker.Add(new AntiSpinState(knowledge, 20, 5, spinMother));
            //dMaker.Add(new AntiSpinState(knowledge, 20, 4, spinMother));
            //dMaker.Add(new AntiSpinState(knowledge, 20, 3, spinMother));
            //dMaker.Add(new AntiSpinState(knowledge, 20, 2, spinMother));
            //dMaker.Add(new AntiSpinState(knowledge, 20, 1, spinMother));
            dMaker.Add(new GoingToCrash(knowledge, ticks));
            
            

            var ai = new RacingAI(dMaker, knowledge);
            //ai.ExecuteLast = new SpinPreventerPrime(knowledge, predictTicks);
            return ai;
        }


        public static RacingAI GeneratePrimeAi(RaceKnowledge knowledge, bool learn)
        {
            DecisionMaker dMaker = new DecisionMaker() { DecisionStyle = DecisionStyle.FirstActive };

            knowledge.InitBends(BendDataLibrary.AddBendsPrime());
            if (learn)
            {
                knowledge.EnableLearner = true;
                //dMaker.Add(GenerateFrictionLearner(knowledge));
            }

            //dMaker.Add(new SpinPreventerPrime(knowledge, 15));
            
            dMaker.Add(new TurboDecision(knowledge));
            dMaker.Add(new BouncerState(knowledge));
            dMaker.Add(new SwitchLaneDodge(knowledge));
            //dMaker.Add(new OnlyOneLane(knowledge, 0));

            var bendForce = 0;
            dMaker.Add(new UpcomingCornerStatePrime(knowledge, bendForce));

            dMaker.Add(new OutOfCornerPrime(knowledge));      // 1st
            //dMaker.Add(new HeadingOutOfCornerState(knowledge));
            dMaker.Add(new CorneringStatePrime(knowledge, bendForce));              // 2nd
            dMaker.Add(new FullOnStraightState(knowledge));         // 3rd

            
            var ai = new RacingAI(dMaker, knowledge);
            ai.ExecuteLast = new SpinPreventerPrime(knowledge, 15);
            return ai;
        }

        public static RacingAI GenerateOldPrimeAi(RaceKnowledge knowledge)
        {
            DecisionMaker dMaker = new DecisionMaker() { DecisionStyle = DecisionStyle.FirstActive };

            knowledge.InitBends(BendDataLibrary.AddBendsPrime());

            dMaker.Add(new SwitchLaneDodge(knowledge));
            
            dMaker.Add(new PrepareForSecondCornerState(knowledge));
            dMaker.Add(new TurboDecision(knowledge));
            dMaker.Add(new SpinPreventer(knowledge, 6));
            dMaker.Add(new HeadingOutOfCornerState(knowledge) { BewareGettingOutOfControl = false });     // 1st
            dMaker.Add(new CorneringState(knowledge));              // 2nd
            dMaker.Add(new FullOnStraightState(knowledge));         // 3rd

            var ai = new RacingAI(dMaker, knowledge);
            return ai;
        }



        public static RacingAI GeneratePrimeKiller(RaceKnowledge knowledge)
        {
            DecisionMaker dMaker = new DecisionMaker() { DecisionStyle = DecisionStyle.FirstActive };

            knowledge.InitBends(BendDataLibrary.AddBendsPrime());
            dMaker.Add(new SwitchLaneDecision(knowledge));
            //dMaker.Add(new SpinPreventer(knowledge, 4));
            dMaker.Add(new PushACar(knowledge));

            dMaker.Add(new PrepareForSecondCornerState(knowledge));
            dMaker.Add(new HeadingOutOfCornerState(knowledge) { BewareGettingOutOfControl = false });     // 1st
            dMaker.Add(new CorneringStatePrime(knowledge, 500));              // 2nd
            dMaker.Add(new FullOnStraightState(knowledge));         // 3rd

            var ai = new RacingAI(dMaker, knowledge);
            return ai;
        }

        private static RacingAI GenerateMopoAi(RaceKnowledge knowledge)
        {
            DecisionMaker dMaker = new DecisionMaker() { DecisionStyle = DecisionStyle.FirstActive };

            knowledge.InitBends(BendDataLibrary.AddBendsNoInOut());
            dMaker.Add(new SwitchLaneDecision(knowledge));
            dMaker.Add(new PrepareForSecondCornerState(knowledge));
            dMaker.Add(new TurboDecision(knowledge));
            dMaker.Add(new CorneringState(knowledge));
            dMaker.Add(new FullOnStraightState(knowledge));

            var ai = new RacingAI(dMaker, knowledge);
            return ai;
        }

        private static RacingAI GenerateBender(RaceKnowledge knowledge)
        {
            DecisionMaker dMaker = new DecisionMaker() { DecisionStyle = DecisionStyle.FirstActive };

            knowledge.InitBends(BendDataLibrary.AddBendsEarlyBrake());

            dMaker.Add(new PrepareForSecondCornerState(knowledge));
            dMaker.Add(new HeadingOutOfCornerState(knowledge) { BewareGettingOutOfControl = true });
            dMaker.Add(new CorneringState(knowledge));
            dMaker.Add(new FullOnStraightState(knowledge));

            var ai = new RacingAI(dMaker, knowledge);
            return ai;
        }

        private static RacingAI GenerateBlocker(RaceKnowledge knowledge)
        {
            DecisionMaker dMaker = new DecisionMaker() { DecisionStyle = DecisionStyle.FirstActive };

            knowledge.InitBends(BendDataLibrary.AddBendsSlow());
            dMaker.Add(new BlockerState(knowledge));
            dMaker.Add(new PrepareForSecondCornerState(knowledge));
            dMaker.Add(new TurboDecision(knowledge));
            dMaker.Add(new HeadingOutOfCornerState(knowledge) { BewareGettingOutOfControl = false });
            dMaker.Add(new CorneringState(knowledge));
            dMaker.Add(new FullOnStraightState(knowledge));

            var ai = new RacingAI(dMaker, knowledge);
            return ai;
        }

        private static RacingAI GenerateTheOne(RaceKnowledge knowledge, int laneNumber)
        {
            DecisionMaker dMaker = new DecisionMaker() { DecisionStyle = DecisionStyle.FirstActive };

            knowledge.InitBends(BendDataLibrary.AddBendsEarlyBrake());
            dMaker.Add(new OnlyOneLane(knowledge, laneNumber));
            dMaker.Add(new PrepareForSecondCornerState(knowledge));
            dMaker.Add(new TurboDecision(knowledge));
            dMaker.Add(new CorneringState(knowledge));
            dMaker.Add(new FullOnStraightState(knowledge));

            var ai = new RacingAI(dMaker, knowledge);
            return ai;
        }


        // PLACE TEST AI HERE:
        private static RacingAI GenerateBrakeTestAi(RaceKnowledge knowledge)
        {

            DecisionMaker dMaker = new DecisionMaker() { DecisionStyle = DecisionStyle.FirstActive };

            knowledge.InitBends(BendDataLibrary.AddBendsPrime());

            dMaker.Add(new HardCodedSwitchLaneDecision(knowledge));
            dMaker.Add(new BrakeTestState(knowledge));      // 1st
            dMaker.Add(new CorneringState(knowledge));      // 2st
            dMaker.Add(new FullOnStraightState(knowledge)); // 3rd

            var ai = new RacingAI(dMaker, knowledge);
            return ai;
        }


        private static RacingAI GenerateConstThrottleAi(RaceKnowledge knowledge, double throttle)
        {
            DecisionMaker dMaker = new DecisionMaker() { DecisionStyle = DecisionStyle.FirstActive };

            //dMaker.Add(new HardCodedSwitchLaneDecision(knowledge));
            //dMaker.Add(new OnlyOneLane(knowledge, 1));
            //dMaker.Add(new SwitchLaneDecision(knowledge));
            dMaker.Add(new ConstThrottleState(knowledge, throttle));

            var ai = new RacingAI(dMaker, knowledge);
            return ai;
        }
        //1151.995
        private static RacingAI GenerateCornerTestAi(RaceKnowledge knowledge, int additionalValue)
        {
            int cornerFc = additionalValue;

            DecisionMaker dMaker = new DecisionMaker() { DecisionStyle = DecisionStyle.FirstActive };

            //dMaker.Add(GenerateFrictionLearner(knowledge));

            knowledge.InitBends(BendDataLibrary.AddBendsPrime());
            //dMaker.Add(new SwitchLaneDecision(knowledge));
            dMaker.Add(new PrepareForSecondCornerState(knowledge));
            dMaker.Add(new TurboDecision(knowledge));
            //dMaker.Add(new HeadingOutOfCornerState(knowledge) { BewareGettingOutOfControl = false });
            dMaker.Add(new SecondCornerStateSolid(knowledge, cornerFc) { BasedOnFk = true});
            dMaker.Add(new CorneringStateSolid(knowledge, cornerFc) { BasedOnFk = true });
            dMaker.Add(new FullOnStraightState(knowledge));
            var ai = new RacingAI(dMaker, knowledge);
            knowledge.Logger.WriteLine("Created CornerTest AI with Fc = " + cornerFc);
            return ai;
        }


        private static IAiState GenerateFrictionLearner(RaceKnowledge knowledge)
        {
            var fLearner = new FrictionForceLearner(knowledge);
            fLearner.Add(new SecondCornerStateSolid(knowledge, fLearner.CurrentFrictionSearch));
            fLearner.Add(new CorneringStateSolid(knowledge, fLearner.CurrentFrictionSearch));
            fLearner.Add(new FullOnStraightState(knowledge)); 
            return fLearner;
        }
    }
}
