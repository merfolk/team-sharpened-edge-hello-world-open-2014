﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RaceBot.Race;
using SEdge.Common;

namespace RaceBot.AI.Decisions
{
    class PushACar : BaseDecision
    {
        public double activated = -1;

        public override bool PassThrough
        {
            get { return false; }
        }
        public PushACar(RaceKnowledge knowledge)
            : base(knowledge)
        {
        }


        public override void CalculateDecisionValue()
        {
            if (!MyCar.IsTurboAvailable && (activated < 0))
            {
                Value = -1;
                return;
            }

            foreach (var car in knowledge.Cars)
            {
                if (car.Equals(MyCar) || car.IsOut) continue;

                var distanceToNextCar = Track.DistanceToNextCar(MyCar, car);
                if (activated > 0 && SameLane(car, MyCar) && distanceToNextCar < 200 && 
                    distanceToNextCar > 0)
                {
                    Value = 1;
                    return;
                }

                if (!car.Equals(MyCar) && SameLane(car, MyCar) &&
                    distanceToNextCar < 100 && distanceToNextCar > 0 &&
                    Track.DistanceToCurrentOrNextBend(MyCar.PiecePosition.pieceIndex,
                    MyCar.PiecePosition.inPieceDistance) < 200 &&
                    !car.IsOut && MyCar.IsTurboAvailable && !MyCar.IsTurboActive)
                {
                    
                    Value = 1;
                    return;
                }
            }
            Value = -1;

        }


        private static bool SameLane(RaceCar car, RaceCar myCar)
        {
            return car.PiecePosition.lane.startLaneIndex ==
                   myCar.PiecePosition.lane.startLaneIndex && car.PiecePosition.lane.endLaneIndex
                   == myCar.PiecePosition.lane.endLaneIndex;
        }

        public override void Execute(IZTime zTime)
        {
            if (MyCar.IsTurboAvailable && !MyCar.IsTurboActive)
            {
                if (activated < 0)
                    activated = zTime.TotalD;
                knowledge.ActivateTurbo();
                Log(zTime, "Push", " TURBO");
            }
            else
            {
                if (zTime.TotalD - activated  > 1)
                {
                    activated = -1;
                    knowledge.Throttle = 0;
                    Log(zTime, "Push", " End");
                }
                else
                {
                    knowledge.Throttle = 1;
                    Log(zTime, "Push");
                }
            }
        }
    }
}
