﻿using System;
using System.Collections.Generic;
using System.Linq;
using RaceBot.Messages.Receive;
using RaceBot.Race;

namespace RaceBot.Utilities
{
    public struct FastLane
    {
        public int Index;
        public double Distance;
    }

    public class LaneComparer : IComparer<FastLane>
    {
        public int Compare(FastLane x, FastLane y)
        {
            return x.Distance.CompareTo(y.Distance);
        }
    }

    /// <summary>
    /// Class for solving the optimum route for a given race track.
    /// </summary>
    public class SwitchLaneSolver
    {
        private RaceTrack raceTrack;
        private const double switchCost = 1;
        /// <summary>
        /// Optimum switches for this track. First int is the switch piece's index,
        /// and second int? is the optimum lane from that switch to the next switch.
        /// </summary>
        private Dictionary<int, int?> optimumSwitches = new Dictionary<int, int?>();

        private LaneComparer laneComparer;


        /// <summary>
        /// Call this constructor as soon as track information is passed in the gameInit message.
        /// The track will then be analyzed.
        /// </summary>
        /// <param name="track">The race track instance.</param>
        public SwitchLaneSolver (RaceTrack track)
        {
            this.raceTrack = track;
            this.Initialize ();
            laneComparer = new  LaneComparer();
        }

        /// <summary>
        /// Initializes optimal lanes for a two-lane track only.
        /// </summary>
        private void Initialize()
        {
            foreach (var piece in raceTrack.Pieces)
            {
                if (piece.HasSwitch)
                {
                    RaceTrackPiece firstSwitch = piece;
                    RaceTrackPiece secondSwitch = piece.GetNextSwitch();

                    int? shortestLane = CalculateShortestLane(firstSwitch, secondSwitch);

                    optimumSwitches.Add (firstSwitch.Index, shortestLane);
                }
            }

            //PrintSwitchIndexesAndOptimalLanes ();
        }


        public int GetOptimumLane(PiecePosition carPosition)
        {
            if (raceTrack.Lanes.Length <= 1)
                return 0;

            int shortestTotalLane;
            double shortestTotalDistance;
            var totalDistances = GetTotalDistances(carPosition, out shortestTotalLane, out shortestTotalDistance);

            if (Math.Abs(shortestTotalDistance - totalDistances[carPosition.lane.endLaneIndex]) < 0.00001)
                return carPosition.lane.endLaneIndex;
            //return shortestLane.HasValue ? shortestLane.Value : carOnPiece.lane.endLaneIndex;
            return shortestTotalLane;
        }


        public FastLane[] GetFastLanes(PiecePosition carPosition)
        {
            if (raceTrack.Lanes.Length <= 1)
                return new[]{new FastLane() };

            int shortestTotalLane;
            double shortestTotalDistance;
            var totalDistances = GetTotalDistances(carPosition, out shortestTotalLane, out shortestTotalDistance);

            var fastLanes = new List<FastLane>(totalDistances.Length);

            for (int i = 0; i < totalDistances.Length; i++)
            {
                fastLanes.Add(new FastLane()
                               {
                                   Distance = totalDistances[i],
                                   Index = i
                               });
            }

            fastLanes.Sort(laneComparer);
            return fastLanes.ToArray();
        }
        

        private double[] GetTotalDistances(PiecePosition carOnPiece, out int shortestTotalLane, out double shortestTotalDistance)
        {
            var stack = new Queue<double[]>();
            List<RaceTrackPiece> switches = new List<RaceTrackPiece>();
            Lane laneSwitch;
            int laneCount = raceTrack.Lanes.Count();
            int numberOfSwitchesAhead = laneCount;

            var firstSwitch = raceTrack.Pieces[carOnPiece.pieceIndex].GetNextSwitch();
            var secondSwitch = firstSwitch.GetNextSwitch();
            for (int j = 0; j < numberOfSwitchesAhead; j++)
            {
                double[] laneLengths = new double[laneCount];
                switches.Add(firstSwitch);
                //var piece = firstSwitch.GetNext();
                for (int lane = 0; lane < laneCount; lane++)
                {
                    var position = new PiecePosition()
                    {
                        inPieceDistance = 0,
                        lane = new Lane() { startLaneIndex = lane, endLaneIndex = lane },
                        lap = 1,
                        pieceIndex = firstSwitch.Index
                    };

                    var distance = raceTrack.DistanceToPiece(secondSwitch.Index, position);

                    laneLengths[lane] = distance;
                }

                stack.Enqueue(laneLengths);
                firstSwitch = secondSwitch;
                secondSwitch = firstSwitch.GetNextSwitch();
            }

            var switchIndex = 0;
            double[] totalDistances = stack.Dequeue();
            for (int i = 0; i < laneCount; i++)
            {
                var currentCost = switchCost * (switchIndex + 1);
                if (switches[switchIndex].Index == 33 && laneCount == 3)
                {
                    currentCost = 20;
                }
                if (i == carOnPiece.lane.endLaneIndex)
                {
                    laneSwitch = new Lane() {startLaneIndex = i, endLaneIndex = i};
                    totalDistances[i] += switches[switchIndex].GetLength(laneSwitch);
                }
                else if (i < carOnPiece.lane.endLaneIndex)
                {
                    laneSwitch = new Lane()
                                 {
                                     startLaneIndex = carOnPiece.lane.endLaneIndex,
                                     endLaneIndex = carOnPiece.lane.endLaneIndex - 1
                                 };
                    totalDistances[i] += switches[switchIndex].GetLength(laneSwitch) + currentCost;
                }
                else if (i > carOnPiece.lane.endLaneIndex)
                {
                    laneSwitch = new Lane()
                                 {
                                     startLaneIndex = carOnPiece.lane.endLaneIndex,
                                     endLaneIndex = carOnPiece.lane.endLaneIndex + 1
                                 };
                    totalDistances[i] += switches[switchIndex].GetLength(laneSwitch) + currentCost;
                }
            }
            switchIndex++;
            while (stack.Count() > 0)
            {
                var laneLengths = stack.Dequeue();

                for (int k = 0; k < laneCount; k++)
                {
                    var currentCost = switchCost * (switchIndex + 1);
                    if (switches[switchIndex].Index == 33 && laneCount == 3)
                    {
                        currentCost = 20;
                    }

                    //int shortestLane = -1;
                    // Current lane
                    laneSwitch = new Lane() {startLaneIndex = k, endLaneIndex = k};
                    double shortestDistance = laneLengths[k] + switches[switchIndex].GetLength(laneSwitch);

                    double d;
                    // Left lane
                    if (k - 1 >= 0)
                    {
                        laneSwitch = new Lane() {startLaneIndex = k, endLaneIndex = k - 1};
                        d = laneLengths[k - 1] + switches[switchIndex].GetLength(laneSwitch) + currentCost;
                        if (d < shortestDistance)
                        {
                            shortestDistance = d;
                        }
                    }
                    // Right lane
                    if (k + 1 < laneCount)
                    {
                        laneSwitch = new Lane() {startLaneIndex = k, endLaneIndex = k + 1};
                        d = laneLengths[k + 1] + switches[switchIndex].GetLength(laneSwitch) + currentCost;
                        if (d < shortestDistance)
                        {
                            shortestDistance = d;
                        }
                    }

                    totalDistances[k] += shortestDistance;
                }
                switchIndex++;
            }

            //
            // Select shortest total distance
            //

            shortestTotalLane = -1;
            shortestTotalDistance = double.MaxValue;

            for (int l = 0; l < laneCount; l++)
            {
                if (totalDistances[l] < shortestTotalDistance)
                {
                    shortestTotalDistance = totalDistances[l];
                    shortestTotalLane = l;
                }
            }
            return totalDistances;
        }


        private List<int> GetSwitchIndexes()
        {
            List<int> switchIndexes = new List<int>();

            foreach (var trackPiece in raceTrack.Pieces)
            {
                if (trackPiece.HasSwitch)
                {
                    switchIndexes.Add(trackPiece.Index);
                }
            }

            return switchIndexes;
        }

        /// <summary>
        /// Calculates shortest lane between two switch pieces.
        /// </summary>
        /// <param name="secondSwitch">Second switch.</param>
        /// <param name="firstSwitch">First switch.</param>
        private int? CalculateShortestLane(RaceTrackPiece firstSwitch, RaceTrackPiece secondSwitch)
        {
            int? shortestLane = null;
            double shortestDistance = Double.MaxValue;
             
            foreach (RaceTrackLane lane in raceTrack.Lanes) 
            {
                var position = new PiecePosition()
                {
                    inPieceDistance = 0,
                    lane = new Lane() { startLaneIndex = lane.Index, endLaneIndex = lane.Index },
                    lap = 1,
                    pieceIndex = firstSwitch.Index
                };
                double distance = raceTrack.DistanceToPiece(secondSwitch.Index, position);
                if (distance < shortestDistance) 
                {
                    shortestDistance = distance;
                    shortestLane = lane.Index;
                }
            }

            return shortestLane;
        }

        /// <summary>
        /// Returns the optimum lane between the next two switch pieces. Returns -1 if no optimum lane could be found.
        /// </summary>
        /// <param name="carOnPiece">Current track piece index that the car is on.</param>
        public int GetOptimumLaneOld (PiecePosition carOnPiece)
        {
            int? optimumLane;
            RaceTrackPiece nextSwitch = this.raceTrack.NextSwitch (carOnPiece.pieceIndex);

            if (nextSwitch != null
                && optimumSwitches.TryGetValue (nextSwitch.GetIndex (), out optimumLane))
            {
                return optimumLane.Value;
            }

            return carOnPiece.lane.endLaneIndex;
        }

        void PrintSwitchIndexesAndOptimalLanes ()
        {
            foreach (KeyValuePair<int, int?> pair in optimumSwitches)
            {
                Console.WriteLine ("Index of switch piece: " + pair.Key + " optimal lane: " + pair.Value);
            }
        }
    }
}

