﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using RaceBot.Messages.Receive;
using RaceBot.Utilities;
using SEdge.Common;

namespace RaceBot.AI.Decisions
{
    public class SpeedRunnerPrime : SpinPreventerPrime
    {
        private readonly double throttle;
        private readonly bool requiresBend;
        private int ticks;
        protected int ticksOnFullSpeed = 20;

        protected override double Throttle
        {
            get
            {
                ticks--;
                if (ticks < 0)
                    return 0;
                return throttle;

            }
            set
            {
                //throttle = value;
                ticks = ticksOnFullSpeed;
            }
        }
        public SpeedRunnerPrime(RaceKnowledge knowledge, int predictTicks, double throttle, bool requiresBend) : base(knowledge, predictTicks)
        {
            this.throttle = throttle;
            this.requiresBend = requiresBend;
            ticksOnFullSpeed = predictTicks;
        }


        public override void CalculateDecisionValue()
        {
            
            var currentTrackPiece = knowledge.MyCar.CurrentTrackPiece;
            if (requiresBend && !currentTrackPiece.IsBend())
            {
                Value = -1;
                return;
            }
            //var y = knowledge.BendLearner.GetBendAdjustmentPiece(MyCar.CurrentTrackPiece.Index).BendEnd;
            //var d = MyCar.CurrentTrackPiece.GetLength(knowledge.PredictLane(MyCar.PiecePosition.pieceIndex)) - MyCar.PiecePosition.inPieceDistance;
            //var piece = MyCar.CurrentTrackPiece.GetNext();
            //while (y >= piece.Index)
            //{
            //    d += piece.GetLength(knowledge.PredictLane(piece.Index));
            //    piece = piece.GetNext();
            //}
            //ticksOnFullSpeed = (int) (d / MyCar.CurrentVelocity) *2;
            Throttle = throttle;
            if (WillCrashOnSpeed(exitWhenStraigth: Straigth))
            {
                Value = 0;
            }
            else
            {
                Value = 1;
            }

            //Value = (ticksTillCrash +throttle + (100 - Math.Abs(lastAngle)) / 100) / 100;
        }


        public override void Execute(IZTime zTime)
        {
            knowledge.Throttle = throttle;
            var s = "R" + ZLib.FloatToText(throttle);
            Log(zTime, s.Replace(",", "").Replace(".", ""), ZLib.FloatToText(maxAngle) + " t:" + ticksTillCrash);
        }
    }
}
