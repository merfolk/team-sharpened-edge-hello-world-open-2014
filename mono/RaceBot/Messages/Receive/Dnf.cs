﻿using System;

namespace RaceBot.Messages.Receive
{
    public class Dnf
    {
        public CarId car;
        public string reason;

        public override string ToString ()
        {
            return String.Format ("DNF: {0}, reason: {1}", car.ToString (), this.reason);
        }
    }
}

