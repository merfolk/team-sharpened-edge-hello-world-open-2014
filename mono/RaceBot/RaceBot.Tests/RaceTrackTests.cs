﻿using System;
using NUnit.Framework;
using RaceBot.Messages.Receive;
using RaceBot.Race;
using System.Linq;
using System.Collections.Generic;
using RaceBot.AI;

namespace RaceBot.Tests
{
    [TestFixture]
    public class RaceTrackTests
    {
        [Test]
        public void KeimolaTrackIsLoadedFromFile()
        {
            // Arrange & Act
            Track track = TestUtils.ReadTestTrackFromFile (TestUtils.PathToKeimolaTrack);

            // Assert
            Assert.IsNotNull (track);
            Assert.AreEqual ("Keimola", track.name);
        }

        [Test]
        public void GermanyTrackIsLoadedFromFile()
        {
            // Arrange & Act
            Track track = TestUtils.ReadTestTrackFromFile (TestUtils.PathToGermanyTrack);

            // Assert
            Assert.IsNotNull (track);
            Assert.AreEqual ("Germany", track.name);
        }

        [Test]
        public void UsaTrackIsLoadedFromFile()
        {
            // Arrange & Act
            Track track = TestUtils.ReadTestTrackFromFile (TestUtils.PathToUsaTrack);

            // Assert
            Assert.IsNotNull (track);
            Assert.AreEqual ("USA", track.name);
        }

        [Test]
        public void FranceTrackIsLoadedFromFile()
        {
            // Arrange & Act
            Track track = TestUtils.ReadTestTrackFromFile (TestUtils.PathToFranceTrack);

            // Assert
            Assert.IsNotNull (track);
            Assert.AreEqual ("France", track.name);
        }

        [Test]
        public void NextTrackPieceIsReturned()
        {
            // Arrange
            var keimola = TestUtils.LoadKeimola();

            // Act & Assert
            Assert.AreEqual(1, keimola.NextTrackPiece(0).Index);
            Assert.AreEqual(2, keimola.NextTrackPiece(1).Index);
            Assert.AreEqual(26, keimola.NextTrackPiece(25).Index);
            Assert.AreEqual(39, keimola.NextTrackPiece(38).Index);
            Assert.AreEqual(0, keimola.NextTrackPiece(39).Index); // Finish line
        }

        [Test]
        public void NextSwitchFromStartOfTrack()
        {
            // Arrange
            var keimola = TestUtils.LoadKeimola();

            // Act
            RaceTrackPiece nextSwitch = keimola.NextSwitch(pieceIndex: 0);

            // Assert
            Assert.IsNotNull(nextSwitch);
            Assert.AreEqual(3, nextSwitch.Index);
        }

        [Test]
        public void KeimolaSwitch2()
        {
            // Arrange
            var keimola = TestUtils.LoadKeimola();

            // Act
            RaceTrackPiece nextSwitch = keimola.NextSwitch(pieceIndex: 4);

            // Assert
            Assert.IsNotNull(nextSwitch);
            Assert.AreEqual(8, nextSwitch.Index);
        }

        [Test]
        public void KeimolaSwitch7()
        {
            // Arrange
            var keimola = TestUtils.LoadKeimola();

            // Act
            RaceTrackPiece nextSwitch = keimola.NextSwitch(pieceIndex: 30);

            // Assert
            Assert.IsNotNull(nextSwitch);
            Assert.AreEqual(35, nextSwitch.Index);
        }

        [Test]
        public void NextSwitchLoopsFromEndOfTrackToFirstSwitch()
        {
            // Arrange
            var keimola = TestUtils.LoadKeimola();

            // Act
            RaceTrackPiece nextSwitch = keimola.NextSwitch(pieceIndex: 39);

            // Assert
            Assert.IsNotNull(nextSwitch);
            Assert.AreEqual(3, nextSwitch.Index);
        }

        [Test]
        public void StraightsKeimola()
        {
            var keimola = TestUtils.LoadKeimola();

            Assert.AreEqual(890, keimola.Straights[0].TotalLength);
            Assert.AreEqual(200, keimola.Straights[1].TotalLength);
            Assert.AreEqual(200, keimola.Straights[2].TotalLength);

        }

        [Test]
        public void StraightsGermany()
        {
            var germany = TestUtils.LoadGermany();

            Assert.AreEqual(459, germany.Straights[0].TotalLength);
            Assert.AreEqual(300, germany.Straights[1].TotalLength);
            Assert.AreEqual(200, germany.Straights[2].TotalLength);
            Assert.AreEqual(200, germany.Straights[3].TotalLength);
        }

        [Test]
        public void StraightsUsa()
        {
            var usa = TestUtils.LoadUsa();

            Assert.AreEqual(600, usa.Straights[0].TotalLength);
            Assert.AreEqual(600, usa.Straights[1].TotalLength);
            Assert.AreEqual(200, usa.Straights[2].TotalLength);
            Assert.AreEqual(200, usa.Straights[3].TotalLength);
        }

        [Test]
        public void StraightsFrance()
        {
            var france = TestUtils.LoadFrance();

            double mainStraight = 94 * 5 + 91 + 102 + 94;
            Assert.AreEqual(mainStraight, france.Straights[0].TotalLength);
        }

        [Test]
        public void StraightsOnACircleTrack()
        {
            Track circle = new Track();

            circle.pieces = new List<Piece>();
            circle.pieces.Add(new Piece { radius = 200, angle = 90f }); // Right turn
            circle.pieces.Add(new Piece { radius = 200, angle = 90f }); // Right turn
            circle.pieces.Add(new Piece { radius = 200, angle = 90f }); // Right turn
            circle.pieces.Add(new Piece { radius = 200, angle = 90f }); // Right turn

            circle.lanes = new List<LaneInit>();
            circle.lanes.Add(new LaneInit { index = 0, distanceFromCenter = 10 });

            RaceTrack raceTrack = new RaceTrack(circle);

            Assert.AreEqual(0, raceTrack.Straights.Count());
            Assert.AreEqual(null, raceTrack.MainStraight);
            Assert.AreEqual(null, raceTrack.SecondBestStraight);
        }

        [Test]
        public void NextSwitchIsNullOnTrackWithNoSwitches()
        {
            var circle = new RaceTrack(TestUtils.CreateCircleTrackWithNoSwitches());

            RaceTrackPiece nextSwitch = circle.NextSwitch(0);

            Assert.IsNull(nextSwitch);

        }

        [Test]
        public void CircleTrackWithNoSwitchesShouldNotThrow()
        {
            // Arrange
            Track circle = TestUtils.CreateCircleTrackWithNoSwitches();

            var piecePositions = new List<PiecePosition>();
            piecePositions.Add(new PiecePosition()
            {
                inPieceDistance = 10,
                lane = new Lane() { endLaneIndex = 0, startLaneIndex = 0 },
                lap = 1,
                pieceIndex = 1
            });
            piecePositions.Add(new PiecePosition()
            {
                inPieceDistance = 20,
                lane = new Lane() { endLaneIndex = 0, startLaneIndex = 0 },
                lap = 1,
                pieceIndex = 1
            });

            RaceKnowledge knowledge = TestUtils.CreateRaceKnowledge(circle, piecePositions);
            knowledge.UpdateTurboAvailable(new TurboAvailable { turboFactor = 3.0, turboDurationMilliseconds = 500, turboDurationTicks = 30 });
            CarPosition[] carPositions = TestUtils.CreateCarPositions(piecePositions);
            RaceBot.Race.RaceTime raceTime = new RaceBot.Race.RaceTime();

            RacingAI AI = AiFactory.GenerateSpinRacerAi(knowledge);
            //RacingAI AI = AiFactory.GeneratePrimeAi(knowledge, learn: true);
            //RacingAI AI = AiFactory.GenerateAi(knowledge, AiFactory.AiType.Bender);

            // Act
            AI.Execute(raceTime, carPositions);

            // Assert
            Console.WriteLine("Still here, no exceptions thrown!?");
            Assert.IsTrue(true); // If an exception is thrown, the test will automatically fail
        }
    }
}

