﻿using System;

namespace RaceBot.Messages.Receive
{
    /// <summary>
    /// Represents the "spawn" message sent by server.
    /// </summary>
    public class Spawn
    {
        public string name;
        public string color;

        public override string ToString ()
        {
            return String.Format ("{0} spawned!", this.name);
        }
    }
}

